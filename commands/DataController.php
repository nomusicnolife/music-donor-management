<?php

namespace app\commands;

use app\models\Confirmation;
use app\models\ConfirmationDonor;
use app\models\ConfirmationTransaction;
use app\models\Donation;
use app\models\Review;
use app\models\Transaction;
use Carbon\Carbon;
use yii\console\Controller;
use yii\console\ExitCode;
use League\Csv\Reader;
use app\models\Donor;
use Stringy\Stringy as S;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class DataController extends Controller
{
    public function actionLoadDonation()
    {
        Donation::deleteAll();

        $csv = Reader::createFromPath(\Yii::getAlias('@app') . '/files/donations.csv', 'r');

        $results = $csv->fetch();

        foreach ($results as $row) {
            $donation = new Donation();
            $donation->timestamp = $row[0];
            $donation->donate_confirm = $row[1];
            $donation->email = $row[3];
            $donation->amount = $row[4];
            $donation->slip_url = $row[6];
            $donation->donate_time = $row[5];
            $donation->note = $row[7];
            $donation->address = $row[8];
            $donation->phone = $row[9];
            $donation->full_name = $row[10];
            $donation->line_id = $row[11];
            $donation->twitter_id = $row[12];
            $donation->take_gift = $row[17];
            $donation->receiving_method = $row[14];
            $donation->picture_type = $row[15];
            $donation->member_selection = $row[16];
            $donation->facebook_id = $row[18];
            $donation->is_continue = $row[19];
            $donation->channel = $row[20];
            $donation->name_signing = $row[21];
            $donation->alias_name = $row[22];
            $donation->message = $row[23];

            if ($donation->save() == false) {
                print_r($donation->errors);
            }
        }

        return ExitCode::OK;
    }

    protected function createDonor($data, $type, $confirmation = null)
    {
        $donorDatum = $data;
        if ($type == Review::USE_DONOR) {
            $donor = new Donor();
        } else if ($type == Review::USE_CONFIRMATION_DONOR) {
            $donor = new ConfirmationDonor();
            $donor->confirmation_id = $confirmation->id;
        }
        $donor->full_name = $donorDatum['name'];
        $donor->address = $donorDatum['address'];
        $donor->email = $donorDatum['email'];

        if (!empty($donorDatum['entryDate'])) {
            $entryDate = Carbon::createFromFormat('d/m/Y H:i:s', $donorDatum['entryDate']);
            $entryDate->year = 2018;
            $donor->entry_date = $entryDate->format('Y-m-d H:i:s');
        }

        $donor->hash = $donorDatum['hash'];
        $donor->is_required_gift = isset($donorDatum['isRequiredGift']) ? $donorDatum['isRequiredGift'] : null;
        $donor->is_valid = $donorDatum['isConfirm'];
        $level = S::create($donorDatum['level']);

        $levelNumber = null;
        if ($level->contains('Music Basic', false)) {
            $levelNumber = 1;
        } else if ($level->contains('Music Backpacker', false)) {
            $levelNumber = 2;
        } else if ($level->contains('Music Fan', false)) {
            $levelNumber = 3;
        } else if ($level->contains('Music Cheerleader', false)) {
            $levelNumber = 4;
        } else if ($level->contains('Music Ranger', false)) {
            $levelNumber = 5;
        } else if ($level->contains('Music Ace Fan', false)) {
            $levelNumber = 6;
        } else if ($level->contains('Music Veteran', false)) {
            $levelNumber = 7;
        } else if ($level->contains('Music Champion', false)) {
            $levelNumber = 8;
        }
        $donor->level = $levelNumber;
        $donor->member_selection = $donorDatum['member'];

        $photoType = S::create($donorDatum['photoType']);
        if ($photoType->isBlank() == false) {
            if ($photoType->contains('เดี่ยว')) {
                $donor->picture_type = Review::PICTURE_TYPE_SOLO;
            } else {
                $donor->picture_type = Review::PICTURE_TYPE_DUO;
            }
        }

        $receivingMethod = S::create($donorDatum['receivingMethod']);
        if ($receivingMethod->isBlank() == false) {
            if ($receivingMethod->contains('รับเอง')) {
                $donor->receiving_method = Review::RECEIVING_METHOD_SELF;
            } else {
                $donor->receiving_method = Review::RECEIVING_METHOD_MAIL;
            }
        }

        $takeGift = S::create($donorDatum['takeGift']);
        if ($takeGift->isBlank() == false) {
            if ($takeGift->contains('รับของที่ระลึกตามเกณฑ์')) {
                $donor->take_gift = 1;
            } else {
                $donor->take_gift = 2;
            }
        }

        $donor->total = $donorDatum['total'];
        $donor->valid_total = $donorDatum['validTotal'];

        if ($donor->save() == false) {
            print_r($donor->errors);
        } else {
            if (!empty($donorDatum['donate'])) {
                $donateData = $donorDatum['donate'];
                /*
                     *         "donate" : [ {
      "comment" : "เอยเอง",
      "date" : "30/04/2018 01:47:00",
      "method" : "บัญชีธนาคาร",
      "valid" : "1",
      "value" : "10000"
    }, {
      "comment" : "",
      "date" : "02/05/2018 17:44:00",
      "method" : "บัญชีธนาคาร",
      "valid" : "1",
      "value" : "10000"
    } ],
                     */

                /*
                 * บัญชีธนาคาร
True Wallet
Paypal
บัญชีธนาคาร (ใหม่) 040-1-43849-1
บัญชีธนาคาร (เก่า) 039-3-22364-2

                 */
                foreach ($donateData as $donateDatum) {
                    if ($type == Review::USE_DONOR) {
                        $transaction = new Transaction();
                        $transaction->donor_id = $donor->id;
                    } else if ($type == Review::USE_CONFIRMATION_DONOR) {
                        $transaction = new ConfirmationTransaction();
                        $transaction->confirmation_donor_id = $donor->id;
                    }

                    $transaction->comment = $donateDatum['comment'];
                    if (!empty($donateDatum['date'])) {
                        $donateDate = Carbon::createFromFormat('d/m/Y H:i:s', $donateDatum['date']);
                        $donateDate->year = 2018;
                        $transaction->date = $donateDate->format('Y-m-d H:i:s');
                    }

                    $method = S::create($donateDatum['method']);
                    $methodNumber = null;
                    if ($method->contains('บัญชีธนาคาร (เก่า)')) {
                        $methodNumber = 1;
                    } else if ($method->contains('บัญชีธนาคาร (ใหม่)')) {
                        $methodNumber = 2;
                    } else if ($method->contains('True Wallet')) {
                        $methodNumber = 3;
                    } else if ($method->contains('Paypal')) {
                        $methodNumber = 4;
                    } else if ($method->contains('บัญชีธนาคาร')) {
                        $methodNumber = 1;
                    }
                    $transaction->method = $methodNumber;

                    $transaction->valid = $donateDatum['valid'];
                    $transaction->value = $donateDatum['value'];

                    if ($transaction->save() == false) {
                        print_r($transaction->errors);
                    }
                }
            }
        }
    }

    public function actionLoadDonor()
    {
        Transaction::deleteAll();
        Donor::deleteAll();

        $contents = \Yii::$app->fs->read('donors.json');
        $donorData = json_decode($contents, true);
        foreach ($donorData as $donorDatum) {
            $this->createDonor($donorDatum, Review::USE_DONOR);
        }

        return ExitCode::OK;
    }

    public function actionLoadConfirmation()
    {
        ConfirmationTransaction::deleteAll();
        ConfirmationDonor::deleteAll();
        Confirmation::deleteAll();

        $contents = \Yii::$app->fs->read('confirms.json');
        $confirmationData = json_decode($contents, true);
        foreach ($confirmationData as $confirmationKey => $confirmationDatum) {
            $confirmation = new Confirmation();
            $confirmation->confirmation_key = $confirmationKey;
            $confirmation->hash = $confirmationDatum['hash'];
            $confirmation->email = $confirmationDatum['email'];

            //June 22nd 2018, 17:14:33
            $confirmationDate = Carbon::createFromFormat('F dS Y, H:i:s', $confirmationDatum['time']);
            $confirmation->confirm_time = $confirmationDate->format('Y-m-d H:i:s');

            if ($confirmation->save() == false) {
                print_r($confirmation->errors);
            } else {
                if (!empty($confirmationDatum['donates'])) {
                    $confirmationDonateData = $confirmationDatum['donates'];
                    foreach ($confirmationDonateData as $confirmationDonateDatum) {
                        $this->createDonor($confirmationDonateDatum, Review::USE_CONFIRMATION_DONOR, $confirmation);
                    }
                }
            }
        }

        return ExitCode::OK;
    }

    public function actionLoadReview()
    {
        $emails = ArrayHelper::getColumn(Donation::find()
            ->select([
                new Expression('LOWER([[email]]) as [[email]]')
            ])
            ->groupBy([
                new Expression('LOWER([[email]])')
            ])
            ->all(), 'email');

        $existingEmails = ArrayHelper::getColumn(Review::find()
            ->andWhere([
                'email' => $emails
            ])
            ->all(), 'email');

        $notExistingEmails = array_diff($emails, $existingEmails);

        foreach ($notExistingEmails as $email) {
            $review = new Review();
            $review->email = $email;
            $review->save();
        }

        return ExitCode::OK;
    }
}