<?php

namespace app\commands;

use app\helpers\ReviewHelper;
use app\models\Confirmation;
use app\models\ConfirmationTransaction;
use app\models\Donation;
use app\models\Donor;
use app\models\Review;
use app\models\ReviewDonor;
use app\models\ReviewTransaction;
use Carbon\Carbon;
use yii\console\Controller;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class ReviewController extends Controller
{
    public function actionPreview()
    {
        $donationCounts = Donation::find()
            ->select([
                new Expression('LOWER(email) as email'),
                new Expression('MIN(CAST(STR_TO_DATE(`donate_time`,\'%d/%m/%Y %H:%i:%s\') AS CHAR)) AS entry_date'),
                new Expression('COUNT(1) AS donation_count'),
                new Expression('BIT_AND(donate_confirm) AS valid')
            ])
            ->groupBy([
                new Expression('LOWER(email)')
            ])
            ->asArray()
            ->all();

        $donations = ArrayHelper::index(Donation::find()
            ->select(
                [
                    'id',
                    'timestamp',
                    'donate_confirm',
                    new Expression('LOWER(email) as email'),
                    'amount',
                    'donate_time',
                    'is_continue',
                    'channel',
                    'slip_url',
                    'full_name',
                    'address',
                    'take_gift',
                    'receiving_method',
                    'picture_type',
                    'member_selection',
                    'phone',
                    'facebook_id',
                    'line_id',
                    'twitter_id',
                    'name_signing',
                    'alias_name',
                    'message',
                    'note'
                ]
            )
            ->all(), 'id', ['email']);

        /* @var Carbon $round1DateTime */
        $round1DateTime = Donation::getRoundTime(1);

        foreach ($donationCounts as $donationCount) {
            $review = Review::find()
                ->andFilterWhere([
                    'email' => trim($donationCount['email'])
                ])
                ->one();

            $confirmation = Confirmation::find()
                ->andFilterWhere([
                    'email' => trim($donationCount['email'])
                ])
                ->one();

            if ($review === null) {
                echo $donationCount['email'] . ' cannot be found';
            } else {
                if ($donationCount['valid'] == 1) {
                    $review->reason_is_all_valid = 1;
                } else {
                    $review->reason_is_all_valid = 0;
                }

                if ($confirmation !== null) {
                    $review->reason_is_confirm = 1;
                    $review->confirmation_key = $confirmation->confirmation_key;
                    $review->hash = $confirmation->hash;
                    $confirmationTime = Carbon::createFromFormat('Y-m-d H:i:s', $confirmation->confirm_time);
                    $review->confirmation_time = $confirmationTime->format('Y-m-d H:i:s');
                    if ($confirmationTime->gt(Confirmation::getDataConfirmationTime())) {
                        $review->reason_is_data_confirm = 1;
                    } else {
                        $review->reason_is_data_confirm = 0;
                    }
                } else {
                    $review->reason_is_confirm = 0;
                }

                if ($donationCount['donation_count'] == 1) {
                    $review->reason_is_single = 1;
                } else {
                    $review->reason_is_single = 0;

                    if (isset($donations[$donationCount['email']])) {
                        $similarAddress = 1;
                        $similarMemberSelecting = 1;
                        $donationData = $donations[$donationCount['email']];

                        if (count($donationData) > 2) {
                            $first = array_shift($donationData);
                            foreach ($donationData as $donationDatum) {
                                if ($first['address'] !== $donationDatum['address']) {
                                    $similarAddress = 0;
                                }

                                if ($first['member_selection'] !== $donationDatum['member_selection']) {
                                    $similarAddress = 0;
                                }
                            }
                        } else {
                            $first = array_shift($donationData);

                            if ($first['address'] !== $donationData[0]['address']) {
                                $similarAddress = 0;
                            }

                            if ($first['member_selection'] !== $donationData[0]['member_selection']) {
                                $similarAddress = 0;
                            }
                        }

                        $review->reason_is_all_similar_address = $similarAddress;
                        $review->reason_is_all_similar_member_selecting = $similarMemberSelecting;
                    }
                }


                try {
                    $entryDateTime = Carbon::createFromFormat('Y-m-d H:i:s', $donationCount['entry_date']);
                    if ($entryDateTime->lessThanOrEqualTo($round1DateTime) == true) {
                        $review->round_number = 1;
                    }
                } catch (\Exception $e) {
                }

                $review->review_process = 'preview';
                if ($review->save() === false) {
                    print_r($review->errors);
                }
            }
        }

        $this->updateClass();
    }

    protected function updateClass()
    {
        $classes = ReviewHelper::getAllClasses();
        foreach ($classes as $class) {
            Review::updateAll(
                [
                    'review_class' => $class
                ],
                ReviewHelper::getClassCondition($class));
        }
    }

    public function actionUpdateClass()
    {
        $this->updateClass();
    }

    public function actionSync($class)
    {
        ReviewHelper::syncByClass($class);
    }

    public function actionSyncAll()
    {
        $classes = ReviewHelper::getAllClasses();
        foreach ($classes as $class) {
            ReviewHelper::syncByClass($class);
        }
    }

//    public function actionSyncSingle()
//    {
//        $reviews = Review::find()
//            ->andWhere([
//                'review_reason' => [
//                    Review::REASON_SINGLE_CONFIRMED,
//                    Review::REASON_SINGLE_UNCONFIRMED,
//                ]
//            ])
//            ->all();
//
//        foreach ($reviews as $review) {
//
//            $confirmation = Confirmation::find()
//                ->andFilterWhere([
//                    'email' => trim($review->email)
//                ])
//                ->one();
//
//            if ($confirmation !== null) {
//                /*
//                 *  *
//                 *  @property string $confirmation_key
// * @property string $email
// * @property string $hash
// * @property string $confirmation_time
// * @property string $review_reason
// * @property string $review_process
// * @property int $is_review
// * @property string $review_time
//                 */
//
//                $review->confirmation_key = $confirmation->confirmation_key;
//                $review->confirmation_time = $confirmation->confirm_time;
//                $review->hash = $confirmation->hash;
//                $review->review_process = Review::PROCESS_SYNCED;
//
//                $confirmationDonors = $confirmation->confirmationDonors;
//                $confirmationDonor = array_shift($confirmationDonors);
//
//                /*
//                 * @property int $review_id
//                 * @property string $full_name
//                 * @property string $address
//                 * @property string $entry_date
//                 * @property int $is_required_gift
//                 * @property int $is_valid
//                 * @property int $level
//                 * @property string $member_selection
//                 * @property int $picture_type
//                 * @property int $receiving_method
//                 * @property int $take_gift
//                 * @property string $total
//                 * @property string $valid_total
//                 * @property string $note
//                 *
//                 */
//
//                $reviewDonor = new ReviewDonor();
//                $reviewDonor->review_id = $review->id;
//                $reviewDonor->full_name = $confirmationDonor->full_name;
//                $reviewDonor->address = $confirmationDonor->address;
//                $reviewDonor->entry_date = $confirmationDonor->entry_date;
//                $reviewDonor->is_required_gift = $confirmationDonor->is_required_gift;
//                $reviewDonor->is_valid = $confirmationDonor->is_valid;
//                $reviewDonor->level = $confirmationDonor->level;
//                $reviewDonor->member_selection = $confirmationDonor->member_selection;
//                $reviewDonor->picture_type = $confirmationDonor->picture_type;
//                $reviewDonor->take_gift = $confirmationDonor->take_gift;
//                $reviewDonor->total = $confirmationDonor->total;
//                $reviewDonor->valid_total = $confirmationDonor->valid_total;
//
//                $confirmationTransactions = $confirmationDonor->confirmationTransactions;
//                /* @var ConfirmationTransaction $confirmationTransaction */
//                $confirmationTransaction = array_shift($confirmationTransactions);
//
//                /*
//                 *  * @property int $review_donor_id
// * @property string $value
// * @property int $method
// * @property string $date
// * @property int $valid
// * @property string $comment
//                 */
//
//                $reviewDonor->note = $confirmationTransaction->comment;
//                if ($reviewDonor->save() === false) {
//                    print_r($reviewDonor->errors);
//                }
//
//                $reviewTransaction = new ReviewTransaction();
//                $reviewTransaction->review_donor_id = $reviewDonor->id;
//                $reviewTransaction->value = $confirmationTransaction->value;
//                $reviewTransaction->method = $confirmationTransaction->method;
//                $reviewTransaction->valid = $confirmationTransaction->valid;
//                $reviewTransaction->comment = $confirmationTransaction->comment;
//                if ($reviewTransaction->save() === false) {
//                    print_r($reviewTransaction->errors);
//                }
//
//                $reviewDonor->is_valid = $confirmationTransaction->valid;
//                if ($review->save() === false) {
//                    print_r($review->errors);
//                }
//            }
//        }
//    }
}