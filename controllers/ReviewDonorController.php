<?php

namespace app\controllers;

use app\helpers\ReviewHelper;
use app\models\Donation;
use app\models\Review;
use Yii;
use app\models\ReviewDonor;
use app\models\ReviewDonorSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReviewDonorController implements the CRUD actions for ReviewDonor model.
 */
class ReviewDonorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ReviewDonor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $phones = ArrayHelper::map(Donation::find()
            ->asArray()
            ->all(), 'email', 'phone');

        $facebooks = ArrayHelper::map(Donation::find()
            ->asArray()
            ->all(), 'email', 'facebook_id');

        $lines = ArrayHelper::map(Donation::find()
            ->asArray()
            ->all(), 'email', 'line_id');

        $searchModel = new ReviewDonorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'phones' => $phones,
            'facebooks' => $facebooks,
            'lines' => $lines,
        ]);
    }

    /**
     * Displays a single ReviewDonor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ReviewDonor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param mixed $reviewId
     * @param mixed $isReview
     * @return mixed
     */
    public function actionCreate($reviewId = null, $isReview = null)
    {
        $model = new ReviewDonor();
        if ($isReview == 1) {
            $model->review_id = $reviewId;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($isReview == 1) {
                return $this->redirect(['review/view', 'id' => $reviewId]);
            } else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'reviewId' => $reviewId,
            'isReview' => $isReview
        ]);
    }

    /**
     * Updates an existing ReviewDonor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param mixed $isReview
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $isReview = null)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($isReview == true) {
                return $this->redirect(['review/view', 'id' => $model->review_id]);
            } else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'isReview' => $isReview
        ]);
    }

    /**
     * Deletes an existing ReviewDonor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param mixed $isReview
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $isReview = null)
    {
        $model = $this->findModel($id);

        if ($isReview == true) {
            $reviewId = $model->review_id;
            $model->delete();
            return $this->redirect(['review/view', 'id' => $reviewId]);
        } else {
            $model->delete();
            return $this->redirect(['index']);
        }
    }

    public function actionUpdateLevel($id, $levelNumber, $isReview = null)
    {
        $model = $this->findModel($id);
        $model->level = $levelNumber;
        $model->presetGift($levelNumber);
        $members = ReviewHelper::getDuoMembers();
        foreach ($members as $member) {
            $model->setAttribute('gift_' . strtolower($member) . '_amount', null);
        }
        $model->gift_music_amount = null;

        if ($model->picture_type == Review::PICTURE_TYPE_DUO) {
            $member = $model->member_selection;
            $model->setAttribute('gift_' . strtolower($member) . '_amount', 1);
        } else if ($model->picture_type == Review::PICTURE_TYPE_SOLO) {
            $model->gift_music_amount = 1;
        }
        $model->save();

        return $this->redirect([
            'update',
            'id' => $id,
            'isReview' => $isReview
        ]);
    }

    /**
     * Finds the ReviewDonor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ReviewDonor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ReviewDonor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
