<?php

namespace app\controllers;

use app\actions\DownloadDonorJsonAction;
use app\actions\DownloadSelfReceivingExcelAction;
use app\helpers\ReviewHelper;
use app\models\Donation;
use app\models\ReviewDonor;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class DataController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'download-donor-json' => DownloadDonorJsonAction::class,
            'download-self-receiving-excel' => DownloadSelfReceivingExcelAction::class,
        ];
    }

    public function actionDownloadAddressLabelExcel($levelNumber)
    {
        ini_set('memory_limit', '2048M');

        $addressLabel = ReviewHelper::getAddressLabel($levelNumber);

        $levelText = ReviewHelper::getLevelText($levelNumber);

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Music 48 Project')
            ->setLastModifiedBy('Music 48 Project')
            ->setTitle('Music Donor Address Label')
            ->setSubject('Music Donor Address Label')
            ->setDescription('Music Donor Address Label')
            ->setKeywords('music donor address label')
            ->setCategory('Export');

        $spreadsheet->setActiveSheetIndex(0);

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setTitle('Address');

        $sheet->setCellValue('A1', 'รหัสอ้างอิง');
        $sheet->setCellValue('B1', 'email');
        $sheet->setCellValue('C1', 'ชื่อ');
        $sheet->setCellValue('D1', 'ที่อยู่');
        $sheet->setCellValue('E1', 'ระดับ');
        $sheet->setCellValue('F1', 'ของที่ระลึก');
        $sheet->setCellValue('G1', 'โทร');
        $sheet->setCellValue('H1', 'กรณี');

        $rowIndex = 2;
        foreach ($addressLabel as $address) {
            $cell1 = $sheet->getCellByColumnAndRow(1, $rowIndex);
            $cell1->setValue($address['รหัสอ้างอิง']);

            $cell2 = $sheet->getCellByColumnAndRow(2, $rowIndex);
            $cell2->setValue($address['email']);

            $cell3 = $sheet->getCellByColumnAndRow(3, $rowIndex);
            $cell3->setValue($address['ชื่อ']);

            $cell4 = $sheet->getCellByColumnAndRow(4, $rowIndex);
            $cell4->setValue($address['ที่อยู่']);

            $cell5 = $sheet->getCellByColumnAndRow(5, $rowIndex);
            $cell5->setValue($address['ระดับ']);

            $cell6 = $sheet->getCellByColumnAndRow(6, $rowIndex);
            $gifts = $address['ของที่ระลึก'];
            $cell6->setValue(implode(' ', $gifts));

            $cell7 = $sheet->getCellByColumnAndRow(7, $rowIndex);
            $cell7->setValue($address['โทร']);

            $cell8 = $sheet->getCellByColumnAndRow(8, $rowIndex);
            $cell8->setValue($address['กรณี']);

            $rowIndex++;
        }

        $levelText = strtolower($levelText);
        $levelText = str_replace(' ', '_', $levelText);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="music_donor_address_label_' . $levelText . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

    public function actionDownloadUnsent()
    {
        ini_set('memory_limit', '2048M');

        $donors = ReviewHelper::getUnsent();

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Music 48 Project')
            ->setLastModifiedBy('Music 48 Project')
            ->setTitle('Music Donor Address Label')
            ->setSubject('Music Donor Address Label')
            ->setDescription('Music Donor Address Label')
            ->setKeywords('music donor address label')
            ->setCategory('Export');

        $spreadsheet->setActiveSheetIndex(0);

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setTitle('Address');

        $sheet->setCellValue('A1', 'รหัสอ้างอิง');
        $sheet->setCellValue('B1', 'email');
        $sheet->setCellValue('C1', 'ชื่อ');
        $sheet->setCellValue('D1', 'ที่อยู่');
        $sheet->setCellValue('E1', 'ระดับ');
        $sheet->setCellValue('F1', 'ของที่ระลึก');
        $sheet->setCellValue('G1', 'โทร');
        $sheet->setCellValue('H1', 'กรณี');
        $sheet->setCellValue('I1', 'วิธีการรับ');

        $rowIndex = 2;
        foreach ($donors as $address) {
            $cell1 = $sheet->getCellByColumnAndRow(1, $rowIndex);
            $cell1->setValue($address['รหัสอ้างอิง']);

            $cell2 = $sheet->getCellByColumnAndRow(2, $rowIndex);
            $cell2->setValue($address['email']);

            $cell3 = $sheet->getCellByColumnAndRow(3, $rowIndex);
            $cell3->setValue($address['ชื่อ']);

            $cell4 = $sheet->getCellByColumnAndRow(4, $rowIndex);
            $cell4->setValue($address['ที่อยู่']);

            $cell5 = $sheet->getCellByColumnAndRow(5, $rowIndex);
            $cell5->setValue($address['ระดับ']);

            $cell6 = $sheet->getCellByColumnAndRow(6, $rowIndex);
            $gifts = $address['ของที่ระลึก'];
            $cell6->setValue(implode(' ', $gifts));

            $cell7 = $sheet->getCellByColumnAndRow(7, $rowIndex);
            $cell7->setValue($address['โทร']);

            $cell8 = $sheet->getCellByColumnAndRow(8, $rowIndex);
            $cell8->setValue($address['กรณี']);

            $cell9 = $sheet->getCellByColumnAndRow(9, $rowIndex);
            $cell9->setValue($address['วิธีการรับ']);

            $rowIndex++;
        }

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="music_donor_gift_unsent.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

    public function actionDownloadDonorExcel()
    {
        ini_set('memory_limit', '2048M');

        $phones = ArrayHelper::map(Donation::find()
            ->asArray()
            ->all(), 'email', 'phone');

        $facebooks = ArrayHelper::map(Donation::find()
            ->asArray()
            ->all(), 'email', 'facebook_id');

        $lines = ArrayHelper::map(Donation::find()
            ->asArray()
            ->all(), 'email', 'line_id');

        $reviewDonorModel = new ReviewDonor();
        $donors = ReviewDonor::find()
            ->joinWith(['review r'], true, 'INNER JOIN')
            ->where(['r.is_review' => true])
            ->all();

        $requiredAttributes = [];

        $requiredAttributes[0] = [
            'id',
        ];

        $requiredAttributes[1] = [
            'full_name',
            'address',
        ];

        $requiredAttributes[2] = [
            'entry_date',
            'is_required_gift_text',
            'is_valid',
            'level_text',
            'member_selection',
            'picture_type_text',
            'receiving_method_text',
            'take_gift_text',
            'total',
            'valid_total',
            'note',
            'to_send_gift_text',
            'gift_sent',
            'mail_tracking_code',
            'gift_wristband_number',
            'gift_round_1_photo_amount',
            'gift_picture_amount',
            'gift_wristband_amount',
            'gift_bag_amount',
            'gift_necklace_amount',
            'gift_keychain_amount',
            'gift_poster_a3_amount',
            'gift_poster_a4_amount',
            'gift_mini_photobook_amount',
            'gift_full_photobook_amount',
            'gift_polo_shirt_amount',
            'gift_polo_shirt_size_s_amount',
            'gift_polo_shirt_size_m_amount',
            'gift_polo_shirt_size_l_amount',
            'gift_polo_shirt_size_xl_amount',
            'gift_polo_shirt_size_2xl_amount',
            'gift_polo_shirt_size_3xl_amount',
            'gift_certificate_amount',
            'gift_akb_cd_amount',
            'gift_cherprang_amount',
            'gift_tarwaan_amount',
            'gift_jennis_amount',
            'gift_pupe_amount',
            'gift_noey_amount',
            'gift_kate_amount',
            'gift_jane_amount',
            'gift_namneung_amount',
            'gift_miori_amount',
            'gift_jaa_amount',
            'gift_kaew_amount',
            'gift_can_amount',
            'gift_mind_amount',
            'gift_orn_amount',
            'gift_namsai_amount',
            'gift_mobile_amount',
            'gift_music_amount',
            'gift_pun_amount',
            'gift_izurina_amount',
            'gift_satchan_amount',
            'gift_jib_amount',
            'gift_korn_amount',
            'gift_kaimook_amount',
            'gift_nink_amount',
            'gift_maysa_amount',
            'gift_piam_amount',
        ];

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Music 48 Project')
            ->setLastModifiedBy('Music 48 Project')
            ->setTitle('Music Donor List')
            ->setSubject('Music Donor List')
            ->setDescription('Music Donor List')
            ->setKeywords('music donor')
            ->setCategory('Export');

        $spreadsheet->setActiveSheetIndex(0);

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('List');

        // Adds headers
        $colIndex = 1;
        foreach ($requiredAttributes[0] as $requiredAttribute) {
            $cell = $sheet->getCellByColumnAndRow($colIndex, 1);
            $cell->setValue($reviewDonorModel->getAttributeLabel($requiredAttribute));
            $colIndex++;
        }

        $cell = $sheet->getCellByColumnAndRow($colIndex, 1);
        $cell->setValue('email');
        $colIndex++;

        foreach ($requiredAttributes[1] as $requiredAttribute) {
            $cell = $sheet->getCellByColumnAndRow($colIndex, 1);
            $cell->setValue($reviewDonorModel->getAttributeLabel($requiredAttribute));
            $colIndex++;
        }

        $cell = $sheet->getCellByColumnAndRow($colIndex, 1);
        $cell->setValue('เบอร์โทร');
        $colIndex++;

        $cell = $sheet->getCellByColumnAndRow($colIndex, 1);
        $cell->setValue('Facebook');
        $colIndex++;

        $cell = $sheet->getCellByColumnAndRow($colIndex, 1);
        $cell->setValue('Line');
        $colIndex++;

        foreach ($requiredAttributes[2] as $requiredAttribute) {
            $cell = $sheet->getCellByColumnAndRow($colIndex, 1);
            $cell->setValue($reviewDonorModel->getAttributeLabel($requiredAttribute));
            $colIndex++;
        }

        // Adds body
        $rowIndex = 2;
        foreach ($donors as $donor) {
            $colIndex = 1;
            foreach ($requiredAttributes[0] as $requiredAttribute) {
                $cell = $sheet->getCellByColumnAndRow($colIndex, $rowIndex);
                $cell->setValue($donor->$requiredAttribute);
                $colIndex++;
            }

            $email = $donor->review->email;
            $cell = $sheet->getCellByColumnAndRow($colIndex, $rowIndex);
            $cell->setValue($email);
            $colIndex++;

            foreach ($requiredAttributes[1] as $requiredAttribute) {
                $cell = $sheet->getCellByColumnAndRow($colIndex, $rowIndex);
                $cell->setValue($donor->$requiredAttribute);
                $colIndex++;
            }

            $phone = '';
            if (isset($phones[$email])) {
                $phone = $phones[$email];
            }
            $cell = $sheet->getCellByColumnAndRow($colIndex, $rowIndex);
            $cell->setValue((string)'0' . $phone);
            $colIndex++;

            $facebook = '';
            if (isset($facebooks[$email])) {
                $facebook = $facebooks[$email];
            }
            $cell = $sheet->getCellByColumnAndRow($colIndex, $rowIndex);
            $cell->setValue($facebook);
            $colIndex++;

            $line = '';
            if (isset($lines[$email])) {
                $line = $lines[$email];
            }
            $cell = $sheet->getCellByColumnAndRow($colIndex, $rowIndex);
            $cell->setValue($line);
            $colIndex++;

            foreach ($requiredAttributes[2] as $requiredAttribute) {
                $cell = $sheet->getCellByColumnAndRow($colIndex, $rowIndex);
                $cell->setValue($donor->$requiredAttribute);
                $colIndex++;
            }
            $rowIndex++;
        }

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="music_donor_list.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

    public function actionDownloadGiftSummaryExcel()
    {
        ini_set('memory_limit', '2048M');

        $result = ReviewHelper::getGiftSummary();

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Music 48 Project')
            ->setLastModifiedBy('Music 48 Project')
            ->setTitle('Music Gift Summary')
            ->setSubject('Music Gift Summary')
            ->setDescription('Music Gift Summary')
            ->setKeywords('music gift')
            ->setCategory('Export');

        $spreadsheet->setActiveSheetIndex(0);

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setTitle('Gift');

        $sheet->setCellValue('A1', 'ของที่ระลึก');
        $sheet->setCellValue('B1', 'จำนวน');

        $rowIndex = 2;
        foreach ($result as $name => $value) {
            $cell1 = $sheet->getCellByColumnAndRow(1, $rowIndex);
            $cell1->setValue($name);

            $cell2 = $sheet->getCellByColumnAndRow(2, $rowIndex);
            $cell2->setValue($value);
            $rowIndex++;
        }

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="music_gift_summary.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

//    public function actionSync($class)
//    {
//        ReviewHelper::syncByClass($class);
//    }
//    public function actionLoadDonation()
//    {
//        Donation::deleteAll();
//
//        $csv = Reader::createFromPath(\Yii::getAlias('@app') . '/files/donations.csv', 'r');
//
//        $results = $csv->fetch();
//
//        foreach ($results as $row) {
//            $donation = new Donation();
//            $donation->timestamp = $row[0];
//            $donation->donate_confirm = $row[1];
//            $donation->email = $row[3];
//            $donation->amount = $row[4];
//            $donation->slip_url = $row[6];
//            $donation->donate_time = $row[5];
//            $donation->note = $row[7];
//            $donation->address = $row[8];
//            $donation->phone = $row[9];
//            $donation->full_name = $row[10];
//            $donation->line_id = $row[11];
//            $donation->twitter_id = $row[12];
//            $donation->take_gift = $row[17];
//            $donation->receiving_method = $row[14];
//            $donation->picture_type = $row[15];
//            $donation->member_selection = $row[16];
//            $donation->facebook_id = $row[18];
//            $donation->is_continue = $row[19];
//            $donation->channel = $row[20];
//            $donation->name_signing = $row[21];
//            $donation->alias_name = $row[22];
//            $donation->message = $row[23];
//
//            if ($donation->save() == false) {
//                print_r($donation->errors);
//            }
//        }
//
//        return 'done';
//    }
//
//    public function actionLoadDonor()
//    {
//        Transaction::deleteAll();
//        Donor::deleteAll();
//
//        $contents = file_get_contents(\Yii::getAlias('@app') . '/files/donors.json', FILE_USE_INCLUDE_PATH);
//        $donorData = json_decode($contents, true);
//        foreach ($donorData as $donorDatum) {
//            $donor = new Donor();
//            $donor->full_name = $donorDatum['name'];
//            $donor->address = $donorDatum['address'];
//            $donor->email = $donorDatum['email'];
//
//            if (!empty($donorDatum['entryDate'])) {
//                $entryDate = Carbon::createFromFormat('d/m/Y H:i:s', $donorDatum['entryDate']);
//                $entryDate->year = 2018;
//                $donor->entry_date = $entryDate->format('Y-m-d H:i:s');
//            }
//
//            $donor->hash = $donorDatum['hash'];
//            $donor->is_required_gift = isset($donorDatum['isRequiredGift']) ? $donorDatum['isRequiredGift'] : null;
//            $donor->is_valid = $donorDatum['isConfirm'];
//            $level = S::create($donorDatum['level']);
//
//            $levelNumber = null;
//            if ($level->contains('Music Basic', false)) {
//                $levelNumber = 1;
//            } else if ($level->contains('Music Backpacker', false)) {
//                $levelNumber = 2;
//            } else if ($level->contains('Music Fan', false)) {
//                $levelNumber = 3;
//            } else if ($level->contains('Music Cheerleader', false)) {
//                $levelNumber = 4;
//            } else if ($level->contains('Music Ranger', false)) {
//                $levelNumber = 5;
//            } else if ($level->contains('Music Ace Fan', false)) {
//                $levelNumber = 6;
//            } else if ($level->contains('Music Veteran', false)) {
//                $levelNumber = 7;
//            } else if ($level->contains('Music Champion', false)) {
//                $levelNumber = 8;
//            }
//            $donor->level = $levelNumber;
//            $donor->member_selection = $donorDatum['member'];
//
//            $photoType = S::create($donorDatum['photoType']);
//            if ($photoType->contains('เดี่ยว')) {
//                $donor->picture_type = 1;
//            } else {
//                $donor->picture_type = 2;
//            }
//
//            $receivingMethod = S::create($donorDatum['receivingMethod']);
//            if ($receivingMethod->contains('รับเอง')) {
//                $donor->receiving_method = 1;
//            } else {
//                $donor->receiving_method = 2;
//            }
//
//            $takeGift = S::create($donorDatum['takeGift']);
//            if ($takeGift->contains('รับของที่ระลึกตามเกณฑ์')) {
//                $donor->take_gift = 1;
//            } else {
//                $donor->take_gift = 2;
//            }
//
//            $donor->total = $donorDatum['total'];
//            $donor->valid_total = $donorDatum['validTotal'];
//
//            if ($donor->save() == false) {
//                print_r($donor->errors);
//            } else {
//                if (!empty($donorDatum['donate'])) {
//                    $donateData = $donorDatum['donate'];
//                    /*
//                         *         "donate" : [ {
//          "comment" : "เอยเอง",
//          "date" : "30/04/2018 01:47:00",
//          "method" : "บัญชีธนาคาร",
//          "valid" : "1",
//          "value" : "10000"
//        }, {
//          "comment" : "",
//          "date" : "02/05/2018 17:44:00",
//          "method" : "บัญชีธนาคาร",
//          "valid" : "1",
//          "value" : "10000"
//        } ],
//                         */
//
//                    /*
//                     * บัญชีธนาคาร
//True Wallet
//Paypal
//บัญชีธนาคาร (ใหม่) 040-1-43849-1
//บัญชีธนาคาร (เก่า) 039-3-22364-2
//
//                     */
//                    foreach ($donateData as $donateDatum) {
//                        $transaction = new Transaction();
//                        $transaction->donor_id = $donor->id;
//                        $transaction->comment = $donateDatum['comment'];
//                        if (!empty($donateDatum['date'])) {
//                            $donateDate = Carbon::createFromFormat('d/m/Y H:i:s', $donateDatum['date']);
//                            $donateDate->year = 2018;
//                            $transaction->date = $donateDate->format('Y-m-d H:i:s');
//                        }
//
//                        $method = S::create($donateDatum['method']);
//                        $methodNumber = null;
//                        if ($method->contains('บัญชีธนาคาร (เก่า)')) {
//                            $methodNumber = 1;
//                        } else if ($method->contains('บัญชีธนาคาร (ใหม่)')) {
//                            $methodNumber = 2;
//                        } else if ($method->contains('True Wallet')) {
//                            $methodNumber = 3;
//                        } else if ($method->contains('Paypal')) {
//                            $methodNumber = 4;
//                        } else if ($method->contains('บัญชีธนาคาร')) {
//                            $methodNumber = 1;
//                        }
//                        $transaction->method = $methodNumber;
//
//                        $transaction->valid = $donateDatum['valid'];
//                        $transaction->value = $donateDatum['value'];
//
//                        if ($transaction->save() == false) {
//                            print_r($transaction->errors);
//                        }
//                    }
//                }
//            }
//        }
//
//        return 'done';
//    }
//
//    public function actionLoadConfirmation()
//    {
//        ConfirmationTransaction::deleteAll();
//        ConfirmationDonor::deleteAll();
//        Confirmation::deleteAll();
//
//        $contents = file_get_contents(\Yii::getAlias('@app') . '/files/confirms.json');
//        $confirmationData = json_decode($contents, true);
//        foreach ($confirmationData as $confirmationKey => $confirmationDatum) {
//            $confirmation = new Confirmation();
//            $confirmation->confirmation_key = $confirmationKey;
//            $confirmation->hash = $confirmationDatum['hash'];
//            $confirmation->email = $confirmationDatum['email'];
//
//            //June 22nd 2018, 17:14:33
//            $confirmationDate = Carbon::createFromFormat('F dS Y, H:i:s', $confirmationDatum['time']);
//            $confirmation->confirm_time = $confirmationDate->format('Y-m-d H:i:s');
//
//            if ($confirmation->save() == false) {
//                print_r($confirmation->errors);
//            } else {
//                if (!empty($confirmationDatum['donates'])) {
//                    $confirmationDonateData = $confirmationDatum['donates'];
//                    foreach ($confirmationDonateData as $confirmationDonateDatum) {
//                        $confirmationDonor = new ConfirmationDonor();
//                        $confirmationDonor->confirmation_id = $confirmation->id;
//                        $confirmationDonor->full_name = $confirmationDonateDatum['name'];
//                        $confirmationDonor->address = $confirmationDonateDatum['address'];
//                        $confirmationDonor->email = $confirmationDonateDatum['email'];
//
//
//                        if (!empty($confirmationDonateDatum['entryDate'])) {
//                            $entryDate = Carbon::createFromFormat('d/m/Y H:i:s', $confirmationDonateDatum['entryDate']);
//                            $entryDate->year = 2018;
//                            $confirmationDonor->entry_date = $entryDate->format('Y-m-d H:i:s');
//                        }
//
//                        $confirmationDonor->hash = $confirmationDonateDatum['hash'];
//                        $confirmationDonor->is_required_gift = isset($confirmationDonateDatum['isRequiredGift']) ? $confirmationDonateDatum['isRequiredGift'] : null;
//                        $confirmationDonor->is_valid = $confirmationDonateDatum['isConfirm'];
//                        $level = S::create($confirmationDonateDatum['level']);
//
//                        $levelNumber = null;
//                        if ($level->contains('Music Basic', false)) {
//                            $levelNumber = 1;
//                        } else if ($level->contains('Music Backpacker', false)) {
//                            $levelNumber = 2;
//                        } else if ($level->contains('Music Fan', false)) {
//                            $levelNumber = 3;
//                        } else if ($level->contains('Music Cheerleader', false)) {
//                            $levelNumber = 4;
//                        } else if ($level->contains('Music Ranger', false)) {
//                            $levelNumber = 5;
//                        } else if ($level->contains('Music Ace Fan', false)) {
//                            $levelNumber = 6;
//                        } else if ($level->contains('Music Veteran', false)) {
//                            $levelNumber = 7;
//                        } else if ($level->contains('Music Champion', false)) {
//                            $levelNumber = 8;
//                        }
//                        $confirmationDonor->level = $levelNumber;
//                        $confirmationDonor->member_selection = $confirmationDonateDatum['member'];
//
//                        $photoType = S::create($confirmationDonateDatum['photoType']);
//                        if ($photoType->contains('เดี่ยว')) {
//                            $confirmationDonor->picture_type = 1;
//                        } else {
//                            $confirmationDonor->picture_type = 2;
//                        }
//
//                        $receivingMethod = S::create($confirmationDonateDatum['receivingMethod']);
//                        if ($receivingMethod->contains('รับเอง')) {
//                            $confirmationDonor->receiving_method = 1;
//                        } else {
//                            $confirmationDonor->receiving_method = 2;
//                        }
//
//                        $takeGift = S::create($confirmationDonateDatum['takeGift']);
//                        if ($takeGift->contains('รับของที่ระลึกตามเกณฑ์')) {
//                            $confirmationDonor->take_gift = 1;
//                        } else {
//                            $confirmationDonor->take_gift = 2;
//                        }
//
//                        $confirmationDonor->total = $confirmationDonateDatum['total'];
//                        $confirmationDonor->valid_total = $confirmationDonateDatum['validTotal'];
//
//                        if ($confirmationDonor->save() == false) {
//                            print_r($confirmationDonor->errors);
//                        } else {
//                            if (!empty($confirmationDonateDatum['donate'])) {
//                                $donateData = $confirmationDonateDatum['donate'];
//                                foreach ($donateData as $donateDatum) {
//                                    $confirmationTransaction = new ConfirmationTransaction();
//                                    $confirmationTransaction->confirmation_donor_id = $confirmationDonor->id;
//                                    $confirmationTransaction->comment = $donateDatum['comment'];
//                                    if (!empty($donateDatum['date'])) {
//                                        $donateDate = Carbon::createFromFormat('d/m/Y H:i:s', $donateDatum['date']);
//                                        $donateDate->year = 2018;
//                                        $confirmationTransaction->date = $donateDate->format('Y-m-d H:i:s');
//                                    }
//
//                                    $method = S::create($donateDatum['method']);
//                                    $methodNumber = null;
//                                    if ($method->contains('บัญชีธนาคาร (เก่า)')) {
//                                        $methodNumber = 1;
//                                    } else if ($method->contains('บัญชีธนาคาร (ใหม่)')) {
//                                        $methodNumber = 2;
//                                    } else if ($method->contains('True Wallet')) {
//                                        $methodNumber = 3;
//                                    } else if ($method->contains('Paypal')) {
//                                        $methodNumber = 4;
//                                    } else if ($method->contains('บัญชีธนาคาร')) {
//                                        $methodNumber = 1;
//                                    }
//                                    $confirmationTransaction->method = $methodNumber;
//
//                                    $confirmationTransaction->valid = $donateDatum['valid'];
//                                    $confirmationTransaction->value = $donateDatum['value'];
//
//                                    if ($confirmationTransaction->save() == false) {
//                                        print_r($confirmationTransaction->errors);
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        return 'done';
//    }
//
//    public function actionLoadEmail()
//    {
//        $emails = ArrayHelper::getColumn(Donation::find()
//            ->select([
//                new Expression('LOWER([[email]]) as [[email]]')
//            ])
//            ->groupBy([
//                new Expression('LOWER([[email]])')
//            ])
//            ->all(), 'email');
//
//        $existingEmails = ArrayHelper::getColumn(Review::find()
//            ->andWhere([
//                'email' => $emails
//            ])
//            ->all(), 'email');
//
//        $notExistingEmails = array_diff($emails, $existingEmails);
//
//        foreach ($notExistingEmails as $email) {
//            $review = new Review();
//            $review->email = $email;
//            $review->save();
//        }
//
//        return 'done';
//    }
}