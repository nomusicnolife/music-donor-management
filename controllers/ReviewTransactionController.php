<?php

namespace app\controllers;

use Yii;
use app\models\ReviewTransaction;
use app\models\ReviewTransactionSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReviewTransactionController implements the CRUD actions for ReviewTransaction model.
 */
class ReviewTransactionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ReviewTransaction models.
     * @return mixed
     */
//    public function actionIndex()
//    {
//        $searchModel = new ReviewTransactionSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }

    /**
     * Displays a single ReviewTransaction model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new ReviewTransaction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($reviewDonorId)
    {
        $model = new ReviewTransaction();
        $model->review_donor_id = $reviewDonorId;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['review/view', 'id' => $model->reviewDonor->review->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ReviewTransaction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $reviewDonorId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $reviewDonorId)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['review/view', 'id' => $model->reviewDonor->review->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'reviewDonorId' => $reviewDonorId
        ]);
    }

    /**
     * Deletes an existing ReviewTransaction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $reviewDonorId
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $reviewDonorId)
    {
        $model = $this->findModel($id);
        $reviewId = $model->reviewDonor->review->id;
        $model->delete();
        return $this->redirect(['review/view', 'id' => $reviewId]);
    }

    /**
     * Finds the ReviewTransaction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ReviewTransaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ReviewTransaction::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
