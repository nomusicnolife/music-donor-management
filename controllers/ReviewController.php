<?php

namespace app\controllers;

use app\helpers\ReviewHelper;
use app\models\Donation;
use app\models\ReviewDonor;
use Carbon\Carbon;
use Yii;
use app\models\Review;
use app\models\ReviewSearch;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Stringy\Stringy as S;

/**
 * ReviewController implements the CRUD actions for Review model.
 */
class ReviewController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionGiftSummary()
    {
        $result = ReviewHelper::getGiftSummary();

        return $this->render('gift_summary', [
            'result' => $result
        ]);
    }

    public function actionAddressLabelSummary($levelNumber)
    {
        $result = ReviewHelper::getAddressLabel($levelNumber);
        $levelText = ReviewHelper::getLevelText($levelNumber);

        return $this->render('address_label_summary', [
            'result' => $result,
            'levelNumber' => $levelNumber,
            'levelText' => $levelText
        ]);
    }

    public function actionSelfReceiving()
    {
        $result = ReviewHelper::getAddressLabel(null, Review::RECEIVING_METHOD_SELF);

        return $this->render('self_receiving', [
            'result' => $result
        ]);
    }

    public function actionSummary()
    {
//        $total = Review::find()->count();

        $summary = Review::find()
            ->select([
                new Expression('SUM(CASE WHEN reason_is_single = 1 AND reason_is_confirm = 1 AND reason_is_data_confirm = 1 THEN 1 ELSE 0 END) AS A'),
                new Expression('SUM(CASE WHEN reason_is_single = 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1 THEN 1 ELSE 0 END) AS B'),
                new Expression('SUM(CASE WHEN reason_is_single = 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1 THEN 1 ELSE 0 END) AS C'),
                new Expression('SUM(CASE WHEN reason_is_single = 1 AND reason_is_confirm != 1 AND reason_is_all_valid != 1 THEN 1 ELSE 0 END) AS D'),
                new Expression('SUM(CASE WHEN reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm = 1 THEN 1 ELSE 0 END) AS E'),
                new Expression('SUM(CASE WHEN reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting = 1 THEN 1 ELSE 0 END) AS F'),
                new Expression('SUM(CASE WHEN reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting != 1 THEN 1 ELSE 0 END) AS G'),
                new Expression('SUM(CASE WHEN reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1 AND reason_is_all_similar_address != 1 THEN 1 ELSE 0 END) AS H'),
                new Expression('SUM(CASE WHEN reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting = 1 THEN 1 ELSE 0 END) AS I'),
                new Expression('SUM(CASE WHEN reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting != 1 THEN 1 ELSE 0 END) AS J'),
                new Expression('SUM(CASE WHEN reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1 AND reason_is_all_similar_address != 1 THEN 1 ELSE 0 END) AS K'),
                new Expression('SUM(CASE WHEN reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid != 1 THEN 1 ELSE 0 END) AS L'),
                // Reviewed
                new Expression('SUM(CASE WHEN is_review = 1 AND reason_is_single = 1 AND reason_is_confirm = 1 AND reason_is_data_confirm = 1 THEN 1 ELSE 0 END) AS A_REVIEWED'),
                new Expression('SUM(CASE WHEN is_review = 1 AND reason_is_single = 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1 THEN 1 ELSE 0 END) AS B_REVIEWED'),
                new Expression('SUM(CASE WHEN is_review = 1 AND reason_is_single = 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1 THEN 1 ELSE 0 END) AS C_REVIEWED'),
                new Expression('SUM(CASE WHEN is_review = 1 AND reason_is_single = 1 AND reason_is_confirm != 1 AND reason_is_all_valid != 1 THEN 1 ELSE 0 END) AS D_REVIEWED'),
                new Expression('SUM(CASE WHEN is_review = 1 AND reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm = 1 THEN 1 ELSE 0 END) AS E_REVIEWED'),
                new Expression('SUM(CASE WHEN is_review = 1 AND reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting = 1 THEN 1 ELSE 0 END) AS F_REVIEWED'),
                new Expression('SUM(CASE WHEN is_review = 1 AND reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting != 1 THEN 1 ELSE 0 END) AS G_REVIEWED'),
                new Expression('SUM(CASE WHEN is_review = 1 AND reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1 AND reason_is_all_similar_address != 1 THEN 1 ELSE 0 END) AS H_REVIEWED'),
                new Expression('SUM(CASE WHEN is_review = 1 AND reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting = 1 THEN 1 ELSE 0 END) AS I_REVIEWED'),
                new Expression('SUM(CASE WHEN is_review = 1 AND reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting != 1 THEN 1 ELSE 0 END) AS J_REVIEWED'),
                new Expression('SUM(CASE WHEN is_review = 1 AND reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1 AND reason_is_all_similar_address != 1 THEN 1 ELSE 0 END) AS K_REVIEWED'),
                new Expression('SUM(CASE WHEN is_review = 1 AND reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid != 1 THEN 1 ELSE 0 END) AS L_REVIEWED'),
            ])
            ->asArray()
            ->all();

//        foreach ($summary[0] as $name => $number) {
//            echo $name . ' : ' . $number . '<br />';
//            $total = $total - $number;
//        }
//        echo 'left : ' . $total;

        $summaryData = $summary[0];

        $conditions = [
            'reason_is_single',
            'reason_is_confirm',
            'reason_is_data_confirm',
            'reason_is_all_valid',
            'reason_is_all_similar_address',
            'reason_is_all_similar_member_selecting',
        ];

        $classes = ReviewHelper::getAllClasses();
        $tableData = [];
        foreach ($classes as $class) {
            $conditionsInClass = explode(' AND ', ReviewHelper::getClassCondition($class));
            $conditionData = [];
            foreach ($conditionsInClass as $conditionInClass) {
                $conditionString = S::create($conditionInClass);
                $conditionName = $conditionString->replace(' = ', '');
                $conditionName = $conditionName->replace(' != ', '');
                $conditionName = $conditionName->replace('1', '');
                $isTrue = 1;
                if ($conditionString->contains('!=')) {
                    $isTrue = 0;
                }
                $conditionData[(string)$conditionName] = $isTrue;
            };

            $row = [];
            foreach ($conditions as $condition) {
                $row[$condition] = isset($conditionData[$condition]) ? $conditionData[$condition] : '';
            }

            $reviewNumber = $summaryData[$class . '_REVIEWED'];
            $row['review'] = $reviewNumber;
            $row['not_review'] = $summaryData[$class] - $reviewNumber;

            $tableData[$class] = $row;
        }

        return $this->render('summary', [
            'summary' => $tableData,
            'conditions' => $conditions
        ]);
    }

    /**
     * Lists all Review models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReviewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Review model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $review = $this->findModel($id);

        // Donations
        $donationQuery = Donation::find()
            ->andFilterWhere([
                'email' => $review->email
            ]);

        $donationProvider = new ActiveDataProvider([
            'query' => $donationQuery,
            'pagination' => false
        ]);

        // Review Donors
        $reviewDonorProvider = new ActiveDataProvider([
            'query' => ReviewDonor::find()
                ->andWhere([
                    'review_id' => $review->id
                ]),
            'pagination' => false
        ]);

        return $this->render('view', [
            'model' => $review,
            'donationProvider' => $donationProvider,
            'reviewDonorProvider' => $reviewDonorProvider
        ]);
    }

    /**
     * Creates a new Review model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Review();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Review model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Review model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Review model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Review the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Review::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionReview($id, $review = 1)
    {
        $model = $this->findModel($id);
        $model->is_review = $review;
        if ($review == 1) {
            $model->review_time = Carbon::now()->format('Y-m-d H:i:s');
            $model->review_process = 'reviewed';
        } else {
            $model->review_time = null;
            $model->review_process = 'rework';
        }

        if ($model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
    }

//    public function actionSetRoundOnePhoto($class)
//    {
//        ReviewHelper::setRoundOnePhoto($class);
//
//        echo 'ok';
//    }

    public function actionGiftSendingSummary()
    {
        return $this->render('gift_sending_summary', [
            'unsentDonors' => ReviewHelper::getUnsent(),
        ]);
    }
}