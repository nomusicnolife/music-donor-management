<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Donor;

/**
 * DonorSearch represents the model behind the search form of `app\models\Donor`.
 */
class DonorSearch extends Donor
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'is_required_gift', 'is_valid', 'level', 'picture_type', 'receiving_method', 'take_gift'], 'integer'],
            [['full_name', 'email', 'address', 'entry_date', 'hash', 'member_selection'], 'safe'],
            [['total', 'valid_total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Donor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'entry_date' => $this->entry_date,
            'is_required_gift' => $this->is_required_gift,
            'is_valid' => $this->is_valid,
            'level' => $this->level,
            'picture_type' => $this->picture_type,
            'receiving_method' => $this->receiving_method,
            'take_gift' => $this->take_gift,
            'total' => $this->total,
            'valid_total' => $this->valid_total,
        ]);

        $query->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'hash', $this->hash])
            ->andFilterWhere(['like', 'member_selection', $this->member_selection]);

        return $dataProvider;
    }
}
