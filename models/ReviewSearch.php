<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Review;

/**
 * ReviewSearch represents the model behind the search form of `app\models\Review`.
 */
class ReviewSearch extends Review
{
    public $has_notes;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'reason_is_single', 'reason_is_confirm', 'reason_is_data_confirm', 'reason_is_all_valid', 'reason_is_all_similar_address', 'reason_is_all_similar_member_selecting', 'round_number', 'is_review'], 'integer'],
            [['confirmation_key', 'email', 'hash', 'confirmation_time', 'review_process', 'all_notes', 'review_class', 'review_time', 'has_notes'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Review::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'confirmation_time' => $this->confirmation_time,
            'reason_is_single' => $this->reason_is_single,
            'reason_is_confirm' => $this->reason_is_confirm,
            'reason_is_data_confirm' => $this->reason_is_data_confirm,
            'reason_is_all_valid' => $this->reason_is_all_valid,
            'reason_is_all_similar_address' => $this->reason_is_all_similar_address,
            'reason_is_all_similar_member_selecting' => $this->reason_is_all_similar_member_selecting,
            'round_number' => $this->round_number,
            'review_time' => $this->review_time,
        ]);

        if ($this->is_review == 1) {
            $query->andWhere([
                'is_review' => 1
            ]);
        } else if ($this->is_review != 1 && $this->is_review != null) {
            $query->andWhere([
                'IS', 'is_review', NULL
            ]);
        }

        if ($this->has_notes == 1) {
            $query->andWhere([
                'IS NOT', 'all_notes', NULL
            ]);
        } else if ($this->has_notes != 1 && $this->has_notes != null) {
            $query->andWhere([
                'IS', 'all_notes', NULL
            ]);
        }

        $query->andFilterWhere(['like', 'confirmation_key', $this->confirmation_key])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'hash', $this->hash])
            ->andFilterWhere(['like', 'review_process', $this->review_process])
            ->andFilterWhere(['like', 'all_notes', $this->all_notes])
            ->andFilterWhere(['like', 'review_class', $this->review_class]);

        return $dataProvider;
    }
}
