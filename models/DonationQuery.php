<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Donation]].
 *
 * @see Donation
 */
class DonationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Donation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Donation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
