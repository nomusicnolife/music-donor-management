<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "donor".
 *
 * @property int $id
 * @property string $full_name
 * @property string $email
 * @property string $address
 * @property string $entry_date
 * @property string $hash
 * @property int $is_required_gift
 * @property int $is_valid
 * @property int $level
 * @property string $member_selection
 * @property int $picture_type
 * @property int $receiving_method
 * @property int $take_gift
 * @property string $total
 * @property string $valid_total
 *
 * @property Transaction[] $transactions
 */
class Donor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%donor}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'string'],
            [['entry_date'], 'safe'],
            [['is_required_gift', 'is_valid', 'level', 'picture_type', 'receiving_method', 'take_gift'], 'integer'],
            [['total', 'valid_total'], 'number'],
            [['full_name', 'email', 'hash', 'member_selection'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'email' => 'Email',
            'address' => 'Address',
            'entry_date' => 'Entry Date',
            'hash' => 'Hash',
            'is_required_gift' => 'Is Required Gift',
            'is_valid' => 'Is Valid',
            'level' => 'Level',
            'member_selection' => 'Member Selection',
            'picture_type' => 'Picture Type',
            'receiving_method' => 'Receiving Method',
            'take_gift' => 'Take Gift',
            'total' => 'Total',
            'valid_total' => 'Valid Total',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['donor_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return DonorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DonorQuery(get_called_class());
    }
}
