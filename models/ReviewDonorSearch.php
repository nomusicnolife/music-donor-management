<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReviewDonor;

/**
 * ReviewDonorSearch represents the model behind the search form of `app\models\ReviewDonor`.
 */
class ReviewDonorSearch extends ReviewDonor
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'review_id', 'is_required_gift', 'is_valid', 'take_gift', 'level', 'picture_type', 'receiving_method', 'to_send_gift', 'gift_sent', 'gift_picture_amount', 'gift_wristband_amount', 'gift_bag_amount', 'gift_necklace_amount', 'gift_keychain_amount', 'gift_poster_a3_amount', 'gift_poster_a4_amount', 'gift_mini_photobook_amount', 'gift_full_photobook_amount', 'gift_polo_shirt_amount', 'gift_polo_shirt_size_s_amount', 'gift_polo_shirt_size_m_amount', 'gift_polo_shirt_size_l_amount', 'gift_polo_shirt_size_xl_amount', 'gift_polo_shirt_size_2xl_amount', 'gift_polo_shirt_size_3xl_amount', 'gift_certificate_amount', 'gift_akb_cd_amount', 'gift_cherprang_amount', 'gift_tarwaan_amount', 'gift_jennis_amount', 'gift_pupe_amount', 'gift_noey_amount', 'gift_kate_amount', 'gift_jane_amount', 'gift_namneung_amount', 'gift_miori_amount', 'gift_jaa_amount', 'gift_kaew_amount', 'gift_can_amount', 'gift_mind_amount', 'gift_orn_amount', 'gift_namsai_amount', 'gift_mobile_amount', 'gift_music_amount', 'gift_pun_amount', 'gift_izurina_amount', 'gift_satchan_amount', 'gift_jib_amount', 'gift_korn_amount', 'gift_kaimook_amount', 'gift_nink_amount', 'gift_maysa_amount', 'gift_piam_amount'], 'integer'],
            [['full_name', 'address', 'entry_date', 'member_selection', 'mail_tracking_code', 'note', 'gift_wristband_number', 'gift_round_1_photo_amount'], 'safe'],
            [['total', 'valid_total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReviewDonor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'review_id' => $this->review_id,
            'entry_date' => $this->entry_date,
            'is_required_gift' => $this->is_required_gift,
            'is_valid' => $this->is_valid,
            'take_gift' => $this->take_gift,
            'level' => $this->level,
            'picture_type' => $this->picture_type,
            'receiving_method' => $this->receiving_method,
            'to_send_gift' => $this->to_send_gift,
            'gift_sent' => $this->gift_sent,
            'total' => $this->total,
            'valid_total' => $this->valid_total,
            'gift_picture_amount' => $this->gift_picture_amount,
            'gift_wristband_amount' => $this->gift_wristband_amount,
            'gift_bag_amount' => $this->gift_bag_amount,
            'gift_necklace_amount' => $this->gift_necklace_amount,
            'gift_keychain_amount' => $this->gift_keychain_amount,
            'gift_poster_a3_amount' => $this->gift_poster_a3_amount,
            'gift_poster_a4_amount' => $this->gift_poster_a4_amount,
            'gift_mini_photobook_amount' => $this->gift_mini_photobook_amount,
            'gift_full_photobook_amount' => $this->gift_full_photobook_amount,
            'gift_polo_shirt_amount' => $this->gift_polo_shirt_amount,
            'gift_polo_shirt_size_s_amount' => $this->gift_polo_shirt_size_s_amount,
            'gift_polo_shirt_size_m_amount' => $this->gift_polo_shirt_size_m_amount,
            'gift_polo_shirt_size_l_amount' => $this->gift_polo_shirt_size_l_amount,
            'gift_polo_shirt_size_xl_amount' => $this->gift_polo_shirt_size_xl_amount,
            'gift_polo_shirt_size_2xl_amount' => $this->gift_polo_shirt_size_2xl_amount,
            'gift_polo_shirt_size_3xl_amount' => $this->gift_polo_shirt_size_3xl_amount,
            'gift_certificate_amount' => $this->gift_certificate_amount,
            'gift_akb_cd_amount' => $this->gift_akb_cd_amount,
            'gift_cherprang_amount' => $this->gift_cherprang_amount,
            'gift_tarwaan_amount' => $this->gift_tarwaan_amount,
            'gift_jennis_amount' => $this->gift_jennis_amount,
            'gift_pupe_amount' => $this->gift_pupe_amount,
            'gift_noey_amount' => $this->gift_noey_amount,
            'gift_kate_amount' => $this->gift_kate_amount,
            'gift_jane_amount' => $this->gift_jane_amount,
            'gift_namneung_amount' => $this->gift_namneung_amount,
            'gift_miori_amount' => $this->gift_miori_amount,
            'gift_jaa_amount' => $this->gift_jaa_amount,
            'gift_kaew_amount' => $this->gift_kaew_amount,
            'gift_can_amount' => $this->gift_can_amount,
            'gift_mind_amount' => $this->gift_mind_amount,
            'gift_orn_amount' => $this->gift_orn_amount,
            'gift_namsai_amount' => $this->gift_namsai_amount,
            'gift_mobile_amount' => $this->gift_mobile_amount,
            'gift_music_amount' => $this->gift_music_amount,
            'gift_pun_amount' => $this->gift_pun_amount,
            'gift_izurina_amount' => $this->gift_izurina_amount,
            'gift_satchan_amount' => $this->gift_satchan_amount,
            'gift_jib_amount' => $this->gift_jib_amount,
            'gift_korn_amount' => $this->gift_korn_amount,
            'gift_kaimook_amount' => $this->gift_kaimook_amount,
            'gift_nink_amount' => $this->gift_nink_amount,
            'gift_maysa_amount' => $this->gift_maysa_amount,
            'gift_piam_amount' => $this->gift_piam_amount,
        ]);

        $query->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'member_selection', $this->member_selection])
            ->andFilterWhere(['like', 'mail_tracking_code', $this->mail_tracking_code])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'gift_wristband_number', $this->gift_wristband_number])
            ->andFilterWhere(['like', 'gift_round_1_photo_amount', $this->gift_round_1_photo_amount]);

        return $dataProvider;
    }
}
