<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ReviewDonor]].
 *
 * @see ReviewDonor
 */
class ReviewDonorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ReviewDonor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ReviewDonor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
