<?php

namespace app\models;

use Carbon\Carbon;
use Yii;

/**
 * This is the model class for table "{{%donation}}".
 *
 * @property int $id
 * @property string $timestamp
 * @property string $donate_confirm
 * @property string $email
 * @property string $amount
 * @property string $donate_time
 * @property string $is_continue
 * @property string $channel
 * @property string $slip_url
 * @property string $full_name
 * @property string $address
 * @property string $take_gift
 * @property string $receiving_method
 * @property string $picture_type
 * @property string $member_selection
 * @property string $phone
 * @property string $facebook_id
 * @property string $line_id
 * @property string $twitter_id
 * @property string $name_signing
 * @property string $alias_name
 * @property string $message
 * @property string $note
 */
class Donation extends \yii\db\ActiveRecord
{
    public static function getRoundTime($roundNumber)
    {
        if ($roundNumber == 1) {
            return Carbon::create(2018, 5, 2, 23, 59, 59);
        }
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%donation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address', 'message', 'note'], 'string'],
            [['timestamp', 'donate_confirm', 'email', 'amount', 'donate_time', 'is_continue', 'channel', 'slip_url', 'full_name', 'take_gift', 'receiving_method', 'picture_type', 'member_selection', 'phone', 'facebook_id', 'line_id', 'twitter_id', 'name_signing', 'alias_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'timestamp' => 'เวลาที่กรอกฟอร์ม',
            'donate_confirm' => 'การโอนถูกต้อง',
            'email' => 'อีเมล์',
            'amount' => 'จำนวน',
            'donate_time' => 'เวลาที่โอน',
            'is_continue' => 'โอนเพิ่ม',
            'channel' => 'ช่องทาง',
            'slip_url' => 'สลิป',
            'full_name' => 'ชื่อและนามสกุล',
            'address' => 'ที่อยู่',
            'take_gift' => 'การรับของที่ระลึก',
            'receiving_method' => 'วิธีการรับ',
            'picture_type' => 'ประเภทรูป',
            'member_selection' => 'สมาชิกที่เลือก',
            'phone' => 'โทรศัพท์',
            'facebook_id' => 'Facebook',
            'line_id' => 'Line',
            'twitter_id' => 'Twitter',
            'name_signing' => 'การลงชื่อ',
            'alias_name' => 'นามปากกา',
            'message' => 'ข้อความ',
            'note' => 'หมายเหตุ',
        ];
    }

    /**
     * {@inheritdoc}
     * @return DonationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DonationQuery(get_called_class());
    }
}
