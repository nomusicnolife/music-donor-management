<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "confirmation_transaction".
 *
 * @property int $id
 * @property int $confirmation_donor_id
 * @property string $value
 * @property int $method
 * @property string $date
 * @property int $valid
 * @property string $comment
 *
 * @property ConfirmationDonor $confirmationDonor
 */
class ConfirmationTransaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%confirmation_transaction}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['confirmation_donor_id'], 'required'],
            [['confirmation_donor_id', 'method', 'valid'], 'integer'],
            [['value'], 'number'],
            [['date'], 'safe'],
            [['comment'], 'string'],
            [['confirmation_donor_id'], 'exist', 'skipOnError' => true, 'targetClass' => ConfirmationDonor::className(), 'targetAttribute' => ['confirmation_donor_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'confirmation_donor_id' => 'Confirmation Donor ID',
            'value' => 'Value',
            'method' => 'Method',
            'date' => 'Date',
            'valid' => 'Valid',
            'comment' => 'Comment',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmationDonor()
    {
        return $this->hasOne(ConfirmationDonor::className(), ['id' => 'confirmation_donor_id']);
    }

    /**
     * {@inheritdoc}
     * @return ConfirmationTransactionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ConfirmationTransactionQuery(get_called_class());
    }
}
