<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%review}}".
 *
 * @property int $id
 * @property string $confirmation_key
 * @property string $email
 * @property string $hash
 * @property string $confirmation_time
 * @property string $review_process
 * @property int $reason_is_single
 * @property int $reason_is_confirm
 * @property int $reason_is_data_confirm
 * @property int $reason_is_all_valid
 * @property int $reason_is_all_similar_address
 * @property int $reason_is_all_similar_member_selecting
 * @property int $round_number
 * @property string $all_notes
 * @property string $review_class
 * @property string $review_time
 * @property int $is_review
 *
 * @property ReviewDonor[] $reviewDonors
 */
class Review extends \yii\db\ActiveRecord
{
    const PICTURE_TYPE_SOLO = 1;
    const PICTURE_TYPE_DUO = 2;

    const RECEIVING_METHOD_SELF = 1;
    const RECEIVING_METHOD_MAIL = 2;

    const TAKE_GIFT_YES = 1;
    const TAKE_GIFT_NO = 2;

    const USE_DONOR = 1;
    const USE_CONFIRMATION_DONOR = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%review}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hash', 'all_notes'], 'string'],
            [['confirmation_time', 'review_time'], 'safe'],
            [['reason_is_single', 'reason_is_confirm', 'reason_is_data_confirm', 'reason_is_all_valid', 'reason_is_all_similar_address', 'reason_is_all_similar_member_selecting', 'round_number', 'is_review'], 'integer'],
            [['confirmation_key', 'email', 'review_process', 'review_class'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'confirmation_key' => 'รหัสเมื่อทำการยืนยัน',
            'email' => 'อีเมล์',
            'hash' => 'รหัสที่ใช้สำหรับยืนยัน',
            'confirmation_time' => 'เวลาที่ยืนยัน',
            'review_process' => 'ขั้นตอนการรีวิว',
            'reason_is_single' => 'กลุ่ม : โอนครั้งเดียว',
            'reason_is_confirm' => 'กลุ่ม : ยืนยันแล้ว',
            'reason_is_data_confirm' => 'กลุ่ม : บันทึกข้อมูลที่ยืนยันแล้ว',
            'reason_is_all_valid' => 'กลุ่ม : ข้อมูลการโอนถูกต้องทุกครั้ง',
            'reason_is_all_similar_address' => 'กลุ่ม : ที่อยู่เหมือนกันทุกครั้ง',
            'reason_is_all_similar_member_selecting' => 'กลุ่ม : รูปเหมือนกันทุกครั้ง',
            'round_number' => 'หมายเลขรอบ',
            'all_notes' => 'หมายเหตุ',
            'review_class' => 'กลุ่มรีวิว',
            'review_time' => 'เวลารีวิว',
            'is_review' => 'ได้ทำการรีวิวแล้ว',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewDonors()
    {
        return $this->hasMany(ReviewDonor::className(), ['review_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ReviewQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReviewQuery(get_called_class());
    }
}
