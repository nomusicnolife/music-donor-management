<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%review_transaction}}".
 *
 * @property int $id
 * @property int $review_donor_id
 * @property string $value
 * @property int $method
 * @property string $method_text
 * @property string $date
 * @property int $valid
 * @property string $comment
 *
 * @property ReviewDonor $reviewDonor
 */
class ReviewTransaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%review_transaction}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['review_donor_id'], 'required'],
            [['review_donor_id', 'method', 'valid'], 'integer'],
            [['value'], 'number'],
            [['date'], 'safe'],
            [['comment'], 'string'],
            [['review_donor_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReviewDonor::className(), 'targetAttribute' => ['review_donor_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'review_donor_id' => 'ข้อมูลสรุปผู้สนับสนุน',
            'value' => 'จำนวน',
            'method' => 'วิธีการโอน',
            'method_text' => 'วิธีการโอน',
            'date' => 'วันที่',
            'valid' => 'การโอนถูกต้อง',
            'comment' => 'หมายเหตุ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewDonor()
    {
        return $this->hasOne(ReviewDonor::className(), ['id' => 'review_donor_id']);
    }

    /**
     * {@inheritdoc}
     * @return ReviewTransactionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReviewTransactionQuery(get_called_class());
    }

    public function getMethod_Text()
    {
        switch ($this->method) {
            case 1:
                $methodText = 'บัญชีธนาคาร (เก่า)';
                break;
            case 2:
                $methodText = 'บัญชีธนาคาร (ใหม่)';
                break;
            case 3:
                $methodText = 'True Wallet';
                break;
            case 4:
                $methodText = 'Paypal';
                break;
            default:
                $methodText = 'ระบุไม่ได้';
        }
        return $methodText;
    }
}
