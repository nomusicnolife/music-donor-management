<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ConfirmationDonor]].
 *
 * @see ConfirmationDonor
 */
class ConfirmationDonorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ConfirmationDonor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ConfirmationDonor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
