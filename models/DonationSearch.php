<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Donation;

/**
 * DonationSearch represents the model behind the search form of `app\models\Donation`.
 */
class DonationSearch extends Donation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['timestamp', 'donate_confirm', 'email', 'amount', 'donate_time', 'is_continue', 'channel', 'slip_url', 'full_name', 'address', 'take_gift', 'receiving_method', 'picture_type', 'member_selection', 'phone', 'facebook_id', 'line_id', 'twitter_id', 'name_signing', 'alias_name', 'message', 'note'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Donation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'timestamp', $this->timestamp])
            ->andFilterWhere(['like', 'donate_confirm', $this->donate_confirm])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'amount', $this->amount])
            ->andFilterWhere(['like', 'donate_time', $this->donate_time])
            ->andFilterWhere(['like', 'is_continue', $this->is_continue])
            ->andFilterWhere(['like', 'channel', $this->channel])
            ->andFilterWhere(['like', 'slip_url', $this->slip_url])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'take_gift', $this->take_gift])
            ->andFilterWhere(['like', 'receiving_method', $this->receiving_method])
            ->andFilterWhere(['like', 'picture_type', $this->picture_type])
            ->andFilterWhere(['like', 'member_selection', $this->member_selection])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'facebook_id', $this->facebook_id])
            ->andFilterWhere(['like', 'line_id', $this->line_id])
            ->andFilterWhere(['like', 'twitter_id', $this->twitter_id])
            ->andFilterWhere(['like', 'name_signing', $this->name_signing])
            ->andFilterWhere(['like', 'alias_name', $this->alias_name])
            ->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
