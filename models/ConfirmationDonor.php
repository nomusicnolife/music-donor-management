<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "confirmation_donor".
 *
 * @property int $id
 * @property int $confirmation_id
 * @property string $full_name
 * @property string $email
 * @property string $address
 * @property string $entry_date
 * @property string $hash
 * @property int $is_required_gift
 * @property int $is_valid
 * @property int $level
 * @property string $member_selection
 * @property int $picture_type
 * @property int $receiving_method
 * @property int $take_gift
 * @property string $total
 * @property string $valid_total
 *
 * @property Confirmation $confirmation
 * @property ConfirmationTransaction[] $confirmationTransactions
 */
class ConfirmationDonor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%confirmation_donor}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['confirmation_id'], 'required'],
            [['confirmation_id', 'is_required_gift', 'is_valid', 'level', 'picture_type', 'receiving_method', 'take_gift'], 'integer'],
            [['address'], 'string'],
            [['entry_date'], 'safe'],
            [['total', 'valid_total'], 'number'],
            [['full_name', 'email', 'hash', 'member_selection'], 'string', 'max' => 255],
            [['confirmation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Confirmation::className(), 'targetAttribute' => ['confirmation_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'confirmation_id' => 'Confirmation ID',
            'full_name' => 'Full Name',
            'email' => 'Email',
            'address' => 'Address',
            'entry_date' => 'Entry Date',
            'hash' => 'Hash',
            'is_required_gift' => 'Is Required Gift',
            'is_valid' => 'Is Valid',
            'level' => 'Level',
            'member_selection' => 'Member Selection',
            'picture_type' => 'Picture Type',
            'receiving_method' => 'Receiving Method',
            'take_gift' => 'Take Gift',
            'total' => 'Total',
            'valid_total' => 'Valid Total',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmation()
    {
        return $this->hasOne(Confirmation::className(), ['id' => 'confirmation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmationTransactions()
    {
        return $this->hasMany(ConfirmationTransaction::className(), ['confirmation_donor_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ConfirmationDonorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ConfirmationDonorQuery(get_called_class());
    }
}
