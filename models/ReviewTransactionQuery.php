<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ReviewTransaction]].
 *
 * @see ReviewTransaction
 */
class ReviewTransactionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ReviewTransaction[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ReviewTransaction|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
