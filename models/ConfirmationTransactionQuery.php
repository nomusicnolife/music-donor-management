<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ConfirmationTransaction]].
 *
 * @see ConfirmationTransaction
 */
class ConfirmationTransactionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ConfirmationTransaction[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ConfirmationTransaction|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
