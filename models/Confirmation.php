<?php

namespace app\models;

use Carbon\Carbon;
use Yii;

/**
 * This is the model class for table "{{%confirmation}}".
 *
 * @property int $id
 * @property string $confirmation_key
 * @property string $email
 * @property string $hash
 * @property string $confirm_time
 *
 * @property ConfirmationDonor[] $confirmationDonors
 */
class Confirmation extends \yii\db\ActiveRecord
{
    public static function getDataConfirmationTime()
    {
        return Carbon::create(2018, 6, 23, 18, 50, 00);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%confirmation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hash'], 'string'],
            [['confirm_time'], 'safe'],
            [['confirmation_key', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'confirmation_key' => 'Confirmation Key',
            'email' => 'Email',
            'hash' => 'Hash',
            'confirm_time' => 'Confirm Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmationDonors()
    {
        return $this->hasMany(ConfirmationDonor::className(), ['confirmation_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ConfirmationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ConfirmationQuery(get_called_class());
    }
}
