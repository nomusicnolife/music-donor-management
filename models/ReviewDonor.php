<?php

namespace app\models;

use app\helpers\ReviewHelper;
use Yii;

/**
 * This is the model class for table "{{%review_donor}}".
 *
 * @property int $id
 * @property int $review_id
 * @property string $full_name
 * @property string $address
 * @property string $entry_date
 * @property int $is_required_gift
 * @property string $is_required_gift_text
 * @property int $is_valid
 * @property int $take_gift
 * @property string $take_gift_text
 * @property int $level
 * @property string $level_text
 * @property string $member_selection
 * @property int $picture_type
 * @property string $picture_type_text
 * @property int $receiving_method
 * @property string $receiving_method_text
 * @property int $to_send_gift
 * @property string $to_send_gift_text
 * @property int $gift_sent
 * @property string $mail_tracking_code
 * @property string $total
 * @property string $valid_total
 * @property string $note
 * @property string $gift_wristband_number
 * @property string $gift_round_1_photo_amount
 * @property int $gift_picture_amount
 * @property int $gift_wristband_amount
 * @property int $gift_bag_amount
 * @property int $gift_necklace_amount
 * @property int $gift_keychain_amount
 * @property int $gift_poster_a3_amount
 * @property int $gift_poster_a4_amount
 * @property int $gift_mini_photobook_amount
 * @property int $gift_full_photobook_amount
 * @property int $gift_polo_shirt_amount
 * @property int $gift_polo_shirt_size_s_amount
 * @property int $gift_polo_shirt_size_m_amount
 * @property int $gift_polo_shirt_size_l_amount
 * @property int $gift_polo_shirt_size_xl_amount
 * @property int $gift_polo_shirt_size_2xl_amount
 * @property int $gift_polo_shirt_size_3xl_amount
 * @property int $gift_certificate_amount
 * @property int $gift_akb_cd_amount
 * @property int $gift_cherprang_amount
 * @property int $gift_tarwaan_amount
 * @property int $gift_jennis_amount
 * @property int $gift_pupe_amount
 * @property int $gift_noey_amount
 * @property int $gift_kate_amount
 * @property int $gift_jane_amount
 * @property int $gift_namneung_amount
 * @property int $gift_miori_amount
 * @property int $gift_jaa_amount
 * @property int $gift_kaew_amount
 * @property int $gift_can_amount
 * @property int $gift_mind_amount
 * @property int $gift_orn_amount
 * @property int $gift_namsai_amount
 * @property int $gift_mobile_amount
 * @property int $gift_music_amount
 * @property int $gift_pun_amount
 * @property int $gift_izurina_amount
 * @property int $gift_satchan_amount
 * @property int $gift_jib_amount
 * @property int $gift_korn_amount
 * @property int $gift_kaimook_amount
 * @property int $gift_nink_amount
 * @property int $gift_maysa_amount
 * @property int $gift_piam_amount
 *
 * @property Review $review
 * @property ReviewTransaction[] $reviewTransactions
 */
class ReviewDonor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%review_donor}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['review_id'], 'required'],
            [['review_id', 'is_required_gift', 'is_valid', 'take_gift', 'level', 'picture_type', 'receiving_method', 'to_send_gift', 'gift_sent', 'gift_picture_amount', 'gift_wristband_amount', 'gift_bag_amount', 'gift_necklace_amount', 'gift_keychain_amount', 'gift_poster_a3_amount', 'gift_poster_a4_amount', 'gift_mini_photobook_amount', 'gift_full_photobook_amount', 'gift_polo_shirt_amount', 'gift_polo_shirt_size_s_amount', 'gift_polo_shirt_size_m_amount', 'gift_polo_shirt_size_l_amount', 'gift_polo_shirt_size_xl_amount', 'gift_polo_shirt_size_2xl_amount', 'gift_polo_shirt_size_3xl_amount', 'gift_certificate_amount', 'gift_akb_cd_amount', 'gift_cherprang_amount', 'gift_tarwaan_amount', 'gift_jennis_amount', 'gift_pupe_amount', 'gift_noey_amount', 'gift_kate_amount', 'gift_jane_amount', 'gift_namneung_amount', 'gift_miori_amount', 'gift_jaa_amount', 'gift_kaew_amount', 'gift_can_amount', 'gift_mind_amount', 'gift_orn_amount', 'gift_namsai_amount', 'gift_mobile_amount', 'gift_music_amount', 'gift_pun_amount', 'gift_izurina_amount', 'gift_satchan_amount', 'gift_jib_amount', 'gift_korn_amount', 'gift_kaimook_amount', 'gift_nink_amount', 'gift_maysa_amount', 'gift_piam_amount'], 'integer'],
            [['address', 'note'], 'string'],
            [['entry_date'], 'safe'],
            [['total', 'valid_total'], 'number'],
            [['full_name', 'member_selection', 'mail_tracking_code', 'gift_wristband_number', 'gift_round_1_photo_amount'], 'string', 'max' => 255],
            [['review_id'], 'exist', 'skipOnError' => true, 'targetClass' => Review::className(), 'targetAttribute' => ['review_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัสอ้างอิง',
            'review_id' => 'รีวิว',
            'full_name' => 'ชื่อและนามสกุล',
            'address' => 'ที่อยู่',
            'entry_date' => 'วันที่สนับสนุนครั้งแรก',
            'is_required_gift' => 'ถึงเกณฑ์',
            'is_required_gift_text' => 'ถึงเกณฑ์',
            'is_valid' => 'การโอนถูกต้องทุกครั้ง',
            'level' => 'ระดับ',
            'level_text' => 'ระดับ',
            'member_selection' => 'รูปคู่สมาชิก',
            'picture_type' => 'ประเภทรูป',
            'picture_type_text' => 'ประเภทรูป',
            'receiving_method' => 'วิธีการรับ',
            'receiving_method_text' => 'วิธีการรับ',
            'take_gift' => 'เลือกรับของที่ระลึก',
            'take_gift_text' => 'เลือกรับของที่ระลึก',
            'total' => 'ยอดรวม',
            'valid_total' => 'ยอดที่ยืนยัน',
            'note' => 'หมายเหตุ',
            'to_send_gift' => 'ต้องส่งของที่ระลึก',
            'to_send_gift_text' => 'ต้องส่งของที่ระลึก',
            'gift_sent' => 'ส่งของที่ระลึกแล้ว',
            'mail_tracking_code' => 'ไปรษณีย์ Tracking Code',
            'gift_wristband_number' => 'หมายเลขริสแบนด์',
            'gift_round_1_photo_amount' => 'จำนวนการ์ดใสรอบแรก',
            'gift_picture_amount' => 'จำนวนรูป',
            'gift_wristband_amount' => 'จำนวนริสแบนด์',
            'gift_bag_amount' => 'จำนวนกระเป๋าผ้า',
            'gift_necklace_amount' => 'จำนวนสายห้อยคอ',
            'gift_keychain_amount' => 'จำนวนพวงกุญแจ',
            'gift_poster_a3_amount' => 'จำนวนโปสเตอร์ A3',
            'gift_poster_a4_amount' => 'จำนวนโปสเตอร์ A4',
            'gift_mini_photobook_amount' => 'จำนวน Mini Photobook',
            'gift_full_photobook_amount' => 'จำนวน Full Photobook',
            'gift_polo_shirt_amount' => 'จำนวนเสื้อโปโล',
            'gift_polo_shirt_size_s_amount' => 'จำนวนเสื้อ Size S',
            'gift_polo_shirt_size_m_amount' => 'จำนวนเสื้อ Size M',
            'gift_polo_shirt_size_l_amount' => 'จำนวนเสื้อ Size L',
            'gift_polo_shirt_size_xl_amount' => 'จำนวนเสื้อ Size XL',
            'gift_polo_shirt_size_2xl_amount' => 'จำนวนเสื้อ Size 2XL',
            'gift_polo_shirt_size_3xl_amount' => 'จำนวนเสื้อ Size 3XL',
            'gift_certificate_amount' => 'จำนวนใบประกาศ',
            'gift_akb_cd_amount' => 'จำนวน CD AKB48',
            'gift_cherprang_amount' => 'จำนวนรูปคู่ Cherprang',
            'gift_tarwaan_amount' => 'จำนวนรูปคู่ Tarwaan',
            'gift_jennis_amount' => 'จำนวนรูปคู่ Jennis',
            'gift_pupe_amount' => 'จำนวนรูปคู่ Pupe',
            'gift_noey_amount' => 'จำนวนรูปคู่ Noey',
            'gift_kate_amount' => 'จำนวนรูปคู่ Kate',
            'gift_jane_amount' => 'จำนวนรูปคู่ Jane',
            'gift_namneung_amount' => 'จำนวนรูปคู่ Namneung',
            'gift_miori_amount' => 'จำนวนรูปคู่ Miori',
            'gift_jaa_amount' => 'จำนวนรูปคู่ Jaa',
            'gift_kaew_amount' => 'จำนวนรูปคู่ Kaew',
            'gift_can_amount' => 'จำนวนรูปคู่ Can',
            'gift_mind_amount' => 'จำนวนรูปคู่ Mind',
            'gift_orn_amount' => 'จำนวนรูปคู่ Orn',
            'gift_namsai_amount' => 'จำนวนรูปคู่ Namsai',
            'gift_mobile_amount' => 'จำนวนรูปคู่ Mobile',
            'gift_music_amount' => 'จำนวนรูปเดี่ยว Music',
            'gift_pun_amount' => 'จำนวนรูปคู่ Pun',
            'gift_izurina_amount' => 'จำนวนรูปคู่ Izurina',
            'gift_satchan_amount' => 'จำนวนรูปคู่ Satchan',
            'gift_jib_amount' => 'จำนวนรูปคู่ Jib',
            'gift_korn_amount' => 'จำนวนรูปคู่ Korn',
            'gift_kaimook_amount' => 'จำนวนรูปคู่ Kaimook',
            'gift_nink_amount' => 'จำนวนรูปคู่ Nink',
            'gift_maysa_amount' => 'จำนวนรูปคู่ Maysa',
            'gift_piam_amount' => 'จำนวนรูปคู่ Piam',
        ];
    }

    public function attributeCodes()
    {
        return [
            'gift_wristband_number' => 'เลข#',
            'gift_round_1_photo_amount' => 'การ์ดใส',
            'gift_wristband_amount' => 'ริสแบนด์',
            'gift_bag_amount' => 'กระเป๋า',
            'gift_necklace_amount' => 'ห้อยคอ',
            'gift_keychain_amount' => 'กุญแจ',
            'gift_poster_a3_amount' => 'โปส3',
            'gift_poster_a4_amount' => 'โปส4',
            'gift_mini_photobook_amount' => 'มินิโฟโต้',
            'gift_full_photobook_amount' => 'โฟโต้เต็ม',
            'gift_polo_shirt_size_s_amount' => 'S',
            'gift_polo_shirt_size_m_amount' => 'M',
            'gift_polo_shirt_size_l_amount' => 'L',
            'gift_polo_shirt_size_xl_amount' => 'XL',
            'gift_polo_shirt_size_2xl_amount' => '2XL',
            'gift_polo_shirt_size_3xl_amount' => '3XL',
            'gift_certificate_amount' => 'ใบประกาศ',
            'gift_akb_cd_amount' => 'CD',
            'gift_cherprang_amount' => 'เฌอ',
            'gift_tarwaan_amount' => 'หวาน',
            'gift_jennis_amount' => 'จอนอ',
            'gift_pupe_amount' => 'เป้',
            'gift_noey_amount' => 'เนย',
            'gift_kate_amount' => 'เคท',
            'gift_jane_amount' => 'เจน',
            'gift_namneung_amount' => 'น้ำหนึ่ง',
            'gift_miori_amount' => 'มี่โกะ',
            'gift_jaa_amount' => 'จ๋า',
            'gift_kaew_amount' => 'แก้ว',
            'gift_can_amount' => 'แคน',
            'gift_mind_amount' => 'มายด์',
            'gift_orn_amount' => 'อร',
            'gift_namsai_amount' => 'น้ำใส',
            'gift_mobile_amount' => 'โม',
            'gift_music_amount' => 'มิว',
            'gift_pun_amount' => 'ปัญ',
            'gift_izurina_amount' => 'รินะ',
            'gift_satchan_amount' => 'ซัทจัง',
            'gift_jib_amount' => 'จิ๊บ',
            'gift_korn_amount' => 'ก่อน',
            'gift_kaimook_amount' => 'ไข่มุก',
            'gift_nink_amount' => 'นิ้ง',
            'gift_maysa_amount' => 'เมษา',
            'gift_piam_amount' => 'เปี่ยม',
        ];
    }

    public function getAttributeCode($attribute)
    {
        $codes = $this->attributeCodes();
        if (isset($codes[$attribute])) {
            return $codes[$attribute];
        }
        return '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReview()
    {
        return $this->hasOne(Review::className(), ['id' => 'review_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewTransactions()
    {
        return $this->hasMany(ReviewTransaction::className(), ['review_donor_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ReviewDonorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReviewDonorQuery(get_called_class());
    }

    public function getIs_Required_Gift_Text()
    {
        if ($this->is_required_gift == 1) {
            return 'ถึงเกณฑ์';
        } else if ($this->is_required_gift == 0) {
            return 'ไม่ถึงเกณฑ์';
        } else {
            return 'สรุปไม่ได้';
        }
    }

    public function getTake_Gift_Text()
    {
        if ($this->take_gift == Review::TAKE_GIFT_YES) {
            return 'ต้องการรับของที่ระลึก';
        } else if ($this->take_gift == Review::TAKE_GIFT_NO) {
            return 'ไม่ต้องการรับของที่ระลึก';
        } else {
            return 'สรุปไม่ได้';
        }
    }

    public function getLevel_Text()
    {
        return ReviewHelper::getLevelText($this->level);
    }

    public function getPicture_Type_Text()
    {
        if ($this->picture_type == Review::PICTURE_TYPE_SOLO) {
            return 'รูปเดี่ยว Music';
        } else if ($this->picture_type == Review::PICTURE_TYPE_DUO) {
            return 'รูปคู่ ' . $this->member_selection;
        }

        return '';
    }

    public function getReceiving_Method_Text()
    {
        if ($this->receiving_method == Review::RECEIVING_METHOD_SELF) {
            return 'รับเอง';
        } else if ($this->receiving_method == Review::RECEIVING_METHOD_MAIL) {
            return 'ส่งไปรษณีย์';
        }

        return '';
    }

    public function getTo_Send_Gift_Text()
    {
        if ($this->to_send_gift == 1) {
            return 'ต้องส่งของที่ระลึก';
        } else if ($this->to_send_gift == 0) {
            return 'ไม่ต้องส่งของที่ระลึก';
        }

        return '';
    }

    public function resetGift()
    {
        $this->gift_picture_amount = 0;
        $this->gift_bag_amount = 0;
        $this->gift_wristband_amount = 0;
        $this->gift_necklace_amount = 0;
        $this->gift_keychain_amount = 0;
        $this->gift_poster_a3_amount = 0;
        $this->gift_polo_shirt_amount = 0;
        $this->gift_akb_cd_amount = 0;
        $this->gift_mini_photobook_amount = 0;
        $this->gift_full_photobook_amount = 0;
    }

    public function presetGift($levelNumber)
    {
        switch ($levelNumber) {
            case 2:
                $this->gift_picture_amount = 1;
                break;
            case 3:
                $this->gift_picture_amount = 1;
                $this->gift_bag_amount = 1;
                break;
            case 4:
                $this->gift_picture_amount = 1;
                $this->gift_wristband_amount = 1;
                break;
            case 5:
                $this->gift_picture_amount = 1;
                $this->gift_bag_amount = 1;
                $this->gift_wristband_amount = 1;
                $this->gift_necklace_amount = 1;
                break;
            case 6:
                $this->gift_picture_amount = 1;
                $this->gift_bag_amount = 1;
                $this->gift_wristband_amount = 1;
                $this->gift_necklace_amount = 1;
                $this->gift_keychain_amount = 1;
                $this->gift_poster_a3_amount = 1;
                break;
            case 7:
                $this->gift_picture_amount = 1;
                $this->gift_bag_amount = 1;
                $this->gift_wristband_amount = 1;
                $this->gift_necklace_amount = 1;
                $this->gift_keychain_amount = 1;
                $this->gift_poster_a3_amount = 1;
                $this->gift_mini_photobook_amount = 1;
                break;
            case 8:
                $this->gift_picture_amount = 1;
                $this->gift_bag_amount = 1;
                $this->gift_wristband_amount = 1;
                $this->gift_necklace_amount = 1;
                $this->gift_keychain_amount = 1;
                $this->gift_poster_a3_amount = 1;
                $this->gift_polo_shirt_amount = 1;
                $this->gift_akb_cd_amount = 1;
                $this->gift_certificate_amount = 1;
                $this->gift_full_photobook_amount = 1;
                break;
            default:
                $this->gift_picture_amount = null;
                $this->gift_bag_amount = null;
                $this->gift_wristband_amount = null;
                $this->gift_necklace_amount = null;
                $this->gift_keychain_amount = null;
                $this->gift_poster_a3_amount = null;
                $this->gift_polo_shirt_amount = null;
                $this->gift_akb_cd_amount = null;
                $this->gift_certificate_amount = null;
                $this->gift_full_photobook_amount = null;
        }
    }

    public function getId_Text()
    {
        return $this->review->email . '#' . $this->id;
    }
}
