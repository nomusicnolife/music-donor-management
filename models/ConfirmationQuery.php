<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Confirmation]].
 *
 * @see Confirmation
 */
class ConfirmationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Confirmation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Confirmation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
