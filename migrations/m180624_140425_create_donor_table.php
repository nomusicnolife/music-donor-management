<?php

use yii\db\Migration;

/**
 * Handles the creation of table `donors`.
 */
class m180624_140425_create_donor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%donor}}', [
            'id' => $this->primaryKey(),
            'full_name' => $this->string(),
            'email' => $this->string(),
            'address' => $this->text(),
            'entry_date' => $this->dateTime(),
            'hash' => $this->string(),
            'is_required_gift' => $this->boolean(),
            'is_valid' => $this->boolean(),
            'level' => $this->integer(),
            'member_selection' => $this->string(),
            'picture_type' => $this->integer(),
            'receiving_method' => $this->integer(),
            'take_gift' => $this->integer(),
            'total' => $this->decimal(10,2),
            'valid_total' => $this->decimal(10,2),
        ], $tableOptions);

        $this->createTable('{{%transaction}}', [
            'id' => $this->primaryKey(),
            'donor_id' => $this->integer()->notNull(),
            'value' => $this->decimal(10,2),
            'method' => $this->integer(),
            'date' => $this->dateTime(),
            'valid' => $this->boolean(),
            'comment' => $this->text()
        ], $tableOptions);

        // creates index for column `author_id`
        $this->createIndex(
            'idx-transaction-donor_id',
            'transaction',
            'donor_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-transaction-author_id',
            'transaction',
            'donor_id',
            'donor',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%transaction}}');
        $this->dropTable('{{%donor}}');
    }
}
