<?php

use yii\db\Migration;

/**
 * Handles the creation of table `reviews`.
 */
class m180624_140813_create_review_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%review}}', [
            'id' => $this->primaryKey(),
            'confirmation_key' => $this->string(),
            'email' => $this->string(),
            'hash' => $this->text(),
            'confirmation_time' => $this->dateTime(),
            'review_process' => $this->string(),
            'reason_is_single' => $this->boolean(),
            'reason_is_confirm' => $this->boolean(),
            'reason_is_data_confirm' => $this->boolean(),
            'reason_is_all_valid' => $this->boolean(),
            'reason_is_all_similar_address' => $this->boolean(),
            'reason_is_all_similar_member_selecting' => $this->boolean(),
            'round_number' => $this->integer(),
            'all_notes' => $this->text(),
            'review_class' => $this->string(),
            'review_time' => $this->dateTime(),
            'is_review' => $this->boolean(),
        ]);

        $this->createTable('{{%review_donor}}', [
            'id' => $this->primaryKey(),
            'review_id' => $this->integer()->notNull(),
            'full_name' => $this->string(),
            'address' => $this->text(),
            'entry_date' => $this->dateTime(),
            'is_required_gift' => $this->boolean(),
            'is_valid' => $this->boolean(),
            'take_gift' => $this->integer(),
            'level' => $this->integer(),
            'member_selection' => $this->string(),
            'picture_type' => $this->integer(),
            'receiving_method' => $this->integer(),
            'to_send_gift' => $this->boolean(),
            'gift_sent' => $this->boolean(),
            'mail_tracking_code' => $this->string(),
            'total' => $this->decimal(10, 2),
            'valid_total' => $this->decimal(10, 2),
            'note' => $this->text(),
            'gift_wristband_number' => $this->string(),
            'gift_round_1_photo_amount' => $this->string(),
            'gift_picture_amount' => $this->integer(),
            'gift_wristband_amount' => $this->integer(),
            'gift_bag_amount' => $this->integer(),
            'gift_necklace_amount' => $this->integer(),
            'gift_keychain_amount' => $this->integer(),
            'gift_poster_a3_amount' => $this->integer(),
            'gift_poster_a4_amount' => $this->integer(),
            'gift_mini_photobook_amount' => $this->integer(),
            'gift_full_photobook_amount' => $this->integer(),
            'gift_polo_shirt_amount' => $this->integer(),
            'gift_polo_shirt_size_s_amount' => $this->integer(),
            'gift_polo_shirt_size_m_amount' => $this->integer(),
            'gift_polo_shirt_size_l_amount' => $this->integer(),
            'gift_polo_shirt_size_xl_amount' => $this->integer(),
            'gift_polo_shirt_size_2xl_amount' => $this->integer(),
            'gift_polo_shirt_size_3xl_amount' => $this->integer(),
            'gift_certificate_amount' => $this->integer(),
            'gift_akb_cd_amount' => $this->integer(),
            'gift_cherprang_amount' => $this->integer(),
            'gift_tarwaan_amount' => $this->integer(),
            'gift_jennis_amount' => $this->integer(),
            'gift_pupe_amount' => $this->integer(),
            'gift_noey_amount' => $this->integer(),
            'gift_kate_amount' => $this->integer(),
            'gift_jane_amount' => $this->integer(),
            'gift_namneung_amount' => $this->integer(),
            'gift_miori_amount' => $this->integer(),
            'gift_jaa_amount' => $this->integer(),
            'gift_kaew_amount' => $this->integer(),
            'gift_can_amount' => $this->integer(),
            'gift_mind_amount' => $this->integer(),
            'gift_orn_amount' => $this->integer(),
            'gift_namsai_amount' => $this->integer(),
            'gift_mobile_amount' => $this->integer(),
            'gift_music_amount' => $this->integer(),
            'gift_pun_amount' => $this->integer(),
            'gift_izurina_amount' => $this->integer(),
            'gift_satchan_amount' => $this->integer(),
            'gift_jib_amount' => $this->integer(),
            'gift_korn_amount' => $this->integer(),
            'gift_kaimook_amount' => $this->integer(),
            'gift_nink_amount' => $this->integer(),
            'gift_maysa_amount' => $this->integer(),
            'gift_piam_amount' => $this->integer(),
        ]);

        $this->createIndex(
            'idx-review_donor_review-id',
            'review_donor',
            'review_id'
        );

        $this->addForeignKey(
            'fk-review_donor_review-id',
            'review_donor',
            'review_id',
            'review',
            'id',
            'CASCADE'
        );

        $this->createTable('{{%review_transaction}}', [
            'id' => $this->primaryKey(),
            'review_donor_id' => $this->integer()->notNull(),
            'value' => $this->decimal(10, 2),
            'method' => $this->integer(),
            'date' => $this->dateTime(),
            'valid' => $this->boolean(),
            'comment' => $this->text()
        ]);

        $this->createIndex(
            'idx-review_transaction_review-donor-id',
            'review_transaction',
            'review_donor_id'
        );

        $this->addForeignKey(
            'fk-review_transaction_review-donor-id',
            'review_transaction',
            'review_donor_id',
            'review_donor',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%review_transaction}}');
        $this->dropTable('{{%review_donor}}');
        $this->dropTable('{{%review}}');
    }
}
