<?php

use yii\db\Migration;

/**
 * Handles the creation of table `donates`.
 */
class m180624_140418_create_donation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%donation}}', [
            'id' => $this->primaryKey(),
            'timestamp' => $this->string(),
            'donate_confirm' => $this->string(),
            'email' => $this->string(),
            'amount' => $this->string(),
            'donate_time' => $this->string(),
            'is_continue' => $this->string(),
            'channel' => $this->string(),
            'slip_url' => $this->string(),
            'full_name' => $this->string(),
            'address' => $this->text(),
            'take_gift' => $this->string(),
            'receiving_method' => $this->string(),
            'picture_type' => $this->string(),
            'member_selection' => $this->string(),
            'phone' => $this->string(),
            'facebook_id' => $this->string(),
            'line_id' => $this->string(),
            'twitter_id' => $this->string(),
            'name_signing' => $this->string(),
            'alias_name' => $this->string(),
            'message' => $this->text(),
            'note' => $this->text()
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%donation}}');
    }
}
