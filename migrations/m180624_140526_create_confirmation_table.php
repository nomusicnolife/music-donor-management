<?php

use yii\db\Migration;

/**
 * Handles the creation of table `confirmations`.
 */
class m180624_140526_create_confirmation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%confirmation}}', [
            'id' => $this->primaryKey(),
            'confirmation_key' => $this->string(),
            'email' => $this->string(),
            'hash' => $this->text(),
            'confirm_time' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable('{{%confirmation_donor}}', [
            'id' => $this->primaryKey(),
            'confirmation_id' => $this->integer()->notNull(),
            'full_name' => $this->string(),
            'email' => $this->string(),
            'address' => $this->text(),
            'entry_date' => $this->dateTime(),
            'hash' => $this->string(),
            'is_required_gift' => $this->tinyInteger(),
            'is_valid' => $this->tinyInteger(),
            'level' => $this->integer(),
            'member_selection' => $this->string(),
            'picture_type' => $this->integer(),
            'receiving_method' => $this->integer(),
            'take_gift' => $this->integer(),
            'total' => $this->decimal(10, 2),
            'valid_total' => $this->decimal(10, 2),
        ], $tableOptions);

        $this->createIndex('idx-confirmation_donor-confirmation_id', '{{%confirmation_donor}}', 'confirmation_id');
        $this->addForeignKey('fk-confirmation_donor-confirmation_id', '{{%confirmation_donor}}', 'confirmation_id', '{{%confirmation}}', 'id', 'CASCADE', 'RESTRICT');

        $this->createTable('{{%confirmation_transaction}}', [
            'id' => $this->primaryKey(),
            'confirmation_donor_id' => $this->integer()->notNull(),
            'value' => $this->decimal(10, 2),
            'method' => $this->integer(),
            'date' => $this->dateTime(),
            'valid' => $this->tinyInteger(),
            'comment' => $this->text(),
        ], $tableOptions);

        $this->createIndex('idx-confirmation_transaction-confirmation_donor_id', '{{%confirmation_transaction}}', 'confirmation_donor_id');
        $this->addForeignKey('fk-confirmation_transaction-confirmation_donor_id', '{{%confirmation_transaction}}', 'confirmation_donor_id', '{{%confirmation_donor}}', 'id', 'CASCADE', 'RESTRICT');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%confirmation_transaction}}');
        $this->dropTable('{{%confirmation_donor}}');
        $this->dropTable('{{%confirmation}}');
    }
}
