<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReviewDonor */

$this->title = 'แก้ไขข้อมูลสรุปผู้สนับสนุน: ' . $model->review->email;
$this->params['breadcrumbs'][] = ['label' => 'ผู้สนับสนุน', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="review-donor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'isReview' => $isReview
    ]) ?>

</div>
