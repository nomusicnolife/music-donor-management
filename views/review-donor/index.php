<?php

use app\helpers\ReviewHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Review;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReviewDonorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลสรุปผู้สนับสนุน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-donor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //echo Html::a('เพิ่มข้อมูลสรุปผู้สนับสนุน', ['create'], ['class' => 'btn btn-success btn-lg']) ?>
        <?php echo
        Html::a(
            'ดาวน์โหลดข้อมูลสรุปผู้สนับสนุน',
            ['data/download-donor-excel'],
            ['class' => 'btn btn-success btn-lg']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => ''
        ],
        'toolbar' => [],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => 'รายชื่อข้อมูลสรุปผู้สนับสนุน',
        ],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'id',
            'review.email',
            'full_name',
            [
                'attribute' => 'address',
                'width' => '180px',
            ],
            [
                'label' => 'โทร',
                'value' => function ($model) use ($phones) {
                    $email = $model->review->email;
                    if (isset($phones[$email])) {
                        return $phones[$email];
                    }
                    return '';
                },
            ],
            [
                'label' => 'Facebook',
                'value' => function ($model) use ($facebooks) {
                    $email = $model->review->email;
                    if (isset($facebooks[$email])) {
                        return $facebooks[$email];
                    }
                    return '';
                },
            ],
            [
                'label' => 'Line',
                'value' => function ($model) use ($lines) {
                    $email = $model->review->email;
                    if (isset($lines[$email])) {
                        return $lines[$email];
                    }
                    return '';
                },
            ],
            'entry_date',
            [
                'attribute' => 'is_required_gift',
                'filter' => [
                    1 => 'ถึงเกณฑ์',
                    0 => 'ไม่ถึงเกณฑ์'
                ],
                'value' => function ($model) {
                    if ($model->is_required_gift == 1) {
                        return 'ถึงเกณฑ์';
                    } else {
                        return 'ไม่ถึงเกณฑ์';
                    }
                },
            ],
            [
                'attribute' => 'is_valid',
                'filter' => [
                    1 => 'ถูกต้องทุกครั้ง',
                    0 => 'บางครั้งไม่ถูกต้อง / ทั้งหมดไม่ถูกต้อง'
                ],
                'value' => function ($model) {
                    if ($model->is_valid == 1) {
                        return 'ถูกต้องทุกครั้ง';
                    } else {
                        return 'บางครั้งไม่ถูกต้อง / ทั้งหมดไม่ถูกต้อง';
                    }
                },
            ],
            [
                'attribute' => 'take_gift',
                'filter' => [
                    1 => 'รับ',
                    0 => 'ไม่รับ'
                ],
                'value' => function ($model) {
                    if ($model->take_gift == 1) {
                        return 'รับ';
                    } else {
                        return 'ไม่รับ';
                    }
                },
            ],
            [
                'attribute' => 'level',
                'filter' => ReviewHelper::getLevels(),
                'value' => function ($model) {
                    $levels = ReviewHelper::getLevels();
                    if (isset($model->level)) {
                        return $levels[$model->level];
                    }
                    return '';
                },
            ],
            [
                'attribute' => 'picture_type',
                'filter' => [
                    Review::PICTURE_TYPE_SOLO => 'รูปเดี่ยว Music',
                    Review::PICTURE_TYPE_DUO => 'รูปคู่'
                ],
                'value' => function ($model) {
                    if ($model->picture_type == Review::PICTURE_TYPE_SOLO) {
                        return 'รูปเดี่ยว Music';
                    } else if ($model->picture_type == Review::PICTURE_TYPE_DUO) {
                        return 'รูปคู่';
                    }
                },
            ],
            [
                'attribute' => 'member_selection',
                'filter' => ReviewHelper::getDuoMembers()
            ],
            [
                'attribute' => 'receiving_method',
                'filter' => [
                    Review::RECEIVING_METHOD_SELF => 'รับเอง (มีโปสเตอร์ A4)',
                    Review::RECEIVING_METHOD_MAIL => 'ส่งไปรษณีย์'
                ],
                'value' => function ($model) {
                    if ($model->receiving_method == Review::RECEIVING_METHOD_SELF) {
                        return 'รับเอง (มีโปสเตอร์ A4)';
                    } else if ($model->receiving_method == Review::RECEIVING_METHOD_MAIL) {
                        return 'ส่งไปรษณีย์';
                    }
                },
            ],
            [
                'attribute' => 'to_send_gift',
                'filter' => [
                    1 => 'ต้องส่งของที่ระลึก',
                    0 => 'ไม่ต้องส่งของที่ระลึก'
                ],
                'value' => function ($model) {
                    if ($model->to_send_gift == 1) {
                        return 'ต้องส่งของที่ระลึก';
                    } else {
                        return 'ไม่ต้องส่งของที่ระลึก';
                    }
                },
            ],
            [
                'attribute' => 'gift_sent',
                'filter' => [
                    1 => 'ส่งแล้ว',
                    0 => 'ยังไม่ได้ส่ง'
                ],
                'value' => function ($model) {
                    if ($model->gift_sent == 1) {
                        return 'ส่งแล้ว';
                    } else {
                        return 'ยังไม่ได้ส่ง';
                    }
                },
            ],
            'mail_tracking_code',
            'total',
            'valid_total',
            'note:ntext',
            'gift_wristband_number',
            'gift_round_1_photo_amount',
            'gift_picture_amount',
            'gift_wristband_amount',
            'gift_bag_amount',
            'gift_necklace_amount',
            'gift_keychain_amount',
            'gift_poster_a3_amount',
            'gift_poster_a4_amount',
            'gift_mini_photobook_amount',
            'gift_full_photobook_amount',
            'gift_polo_shirt_amount',
            'gift_polo_shirt_size_s_amount',
            'gift_polo_shirt_size_m_amount',
            'gift_polo_shirt_size_l_amount',
            'gift_polo_shirt_size_xl_amount',
            'gift_polo_shirt_size_2xl_amount',
            'gift_polo_shirt_size_3xl_amount',
            'gift_certificate_amount',
            'gift_akb_cd_amount',
            'gift_cherprang_amount',
            'gift_tarwaan_amount',
            'gift_jennis_amount',
            'gift_pupe_amount',
            'gift_noey_amount',
            'gift_kate_amount',
            'gift_jane_amount',
            'gift_namneung_amount',
            'gift_miori_amount',
            'gift_jaa_amount',
            'gift_kaew_amount',
            'gift_can_amount',
            'gift_mind_amount',
            'gift_orn_amount',
            'gift_namsai_amount',
            'gift_mobile_amount',
            'gift_music_amount',
            'gift_pun_amount',
            'gift_izurina_amount',
            'gift_satchan_amount',
            'gift_jib_amount',
            'gift_korn_amount',
            'gift_kaimook_amount',
            'gift_nink_amount',
            'gift_maysa_amount',
            'gift_piam_amount',

            ['class' => 'kartik\grid\ActionColumn'],
        ],
    ]); ?>
</div>
