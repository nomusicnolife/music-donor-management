<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Review;
use app\helpers\ReviewHelper;

/* @var $this yii\web\View */
/* @var $model app\models\ReviewDonor */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="review-donor-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success btn-block btn-lg']) ?>
    </div>

    <?php

    $disableReviewSelecting = false;
    if (Yii::$app->user->id != 100) {
        if (!$model->isNewRecord) {
            $disableReviewSelecting = true;
        } else if ($isReview == 1) {
            $disableReviewSelecting = true;
        }
    }

    echo $form->field($model, 'review_id')
        ->widget(Select2::classname(),
            [
                'data' => ArrayHelper::map(\app\models\Review::find()->all(), 'id', 'email'),
                'options' => [
                    'placeholder' => 'เลือกเมล์ ...',
                    'disabled' => $disableReviewSelecting
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
    ?>

    <div class="row">
        <div class="col-md-4">
            <h1>ข้อมูลส่วนตัว</h1>
            <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

            <?php
            echo $form->field($model, 'receiving_method')
                ->widget(Select2::classname(),
                    [
                        'data' => [
                            Review::RECEIVING_METHOD_SELF => 'รับเอง (มีโปสเตอร์ A4)',
                            Review::RECEIVING_METHOD_MAIL => 'ส่งไปรษณีย์'
                        ],
                        'options' => ['placeholder' => 'วิธีการรับ ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
            ?>

            <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-4">
            <h1>ข้อมูลการสนับสนุน</h1>
            <?= $form->field($model, 'entry_date')->textInput() ?>

            <?php
            echo $form->field($model, 'is_required_gift')
                ->widget(Select2::classname(),
                    [
                        'data' => [
                            1 => 'ถึงเกณฑ์',
                            0 => 'ไม่ถึงเกณฑ์'
                        ],
                        'options' => ['placeholder' => 'เลือกเกณฑ์ ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
            ?>

            <?php
            echo $form->field($model, 'take_gift')
                ->widget(Select2::classname(),
                    [
                        'data' => [
                            1 => 'รับ',
                            0 => 'ไม่รับ'
                        ],
                        'options' => ['placeholder' => 'การรับของที่ระลึก ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
            ?>

            <?php
            echo $form->field($model, 'is_valid')
                ->widget(Select2::classname(),
                    [
                        'data' => [
                            1 => 'ถูกต้องทุกครั้ง',
                            0 => 'บางครั้งไม่ถูกต้อง / ทั้งหมดไม่ถูกต้อง'
                        ],
                        'options' => ['placeholder' => 'การยืนยัน ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
            ?>

            <?= $form->field($model, 'total')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'valid_total')->textInput(['maxlength' => true]) ?>

            <h1>ข้อมูลการส่งของที่ระลึก</h1>

            <?php
            echo $form->field($model, 'to_send_gift')
                ->widget(Select2::classname(),
                    [
                        'data' => [
                            1 => 'ต้องส่งของที่ระลึก',
                            0 => 'ไม่ต้องส่งของที่ระลึก'
                        ],
                        'options' => ['placeholder' => 'การส่งของที่ระลึก ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
            ?>

            <?php
            echo $form->field($model, 'gift_sent')
                ->widget(Select2::classname(),
                    [
                        'data' => [
                            1 => 'ส่งแล้ว',
                            0 => 'ยังไม่ได้ส่ง'
                        ],
                        'options' => ['placeholder' => 'ได้ส่งของที่ระลึก ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
            ?>

            <?= $form->field($model, 'mail_tracking_code')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <h1>ข้อมูลของที่ระลึก</h1>
            <?php
            echo $form->field($model, 'level')
                ->widget(Select2::classname(),
                    [
                        'data' => ReviewHelper::getLevels(),
                        'options' => ['placeholder' => 'เลือกระดับ ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
            ?>

            <?= $form->field($model, 'gift_wristband_number')->textInput(['maxlength' => true])->hint('ถ้ามีหลายอันให้ใส่ , คั่น เช่น 12, 55, 765') ?>

            <?= $form->field($model, 'gift_round_1_photo_amount')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'gift_picture_amount')->textInput()->hint('รูป photoset') ?>

            <?php
            echo $form->field($model, 'picture_type')
                ->widget(Select2::classname(),
                    [
                        'data' => [
                            Review::PICTURE_TYPE_SOLO => 'รูปเดี่ยว Music',
                            Review::PICTURE_TYPE_DUO => 'รูปคู่'
                        ],
                        'options' => ['placeholder' => 'เลือกรูป ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
            ?>

            <?php
            echo $form->field($model, 'member_selection')
                ->widget(Select2::classname(),
                    [
                        'data' => ReviewHelper::getDuoMembers(),
                        'options' => ['placeholder' => 'เลือกรูปคู่ ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
            ?>
        </div>
    </div>
    <?php if (!$model->isNewRecord) : ?>
        <div class="row">
            <div class="col-md-12">
                <h1>ปรับระดับและของที่ระลึกอัตโนมัติ</h1>
                <?php
                $levels = ReviewHelper::getLevels();
                foreach ($levels as $levelNumber => $levelText) {
                    echo Html::a(
                            $levelText,
                            [
                                'update-level',
                                'id' => $model->id,
                                'levelNumber' => $levelNumber,
                                'isReview' => $isReview
                            ],
                            [
                                'class' => 'btn btn-primary',
                                'data' => [
                                    'confirm' => 'ต้องการปรับให้เป็น ' . $levelText . ' ?',
                                    'method' => 'post'
                                ],
                            ]) . ' ';
                }
                ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-4">
            <h1>จำนวนของที่ระลึก</h1>
            <?= $form->field($model, 'gift_wristband_amount')->textInput() ?>

            <?= $form->field($model, 'gift_bag_amount')->textInput() ?>

            <?= $form->field($model, 'gift_necklace_amount')->textInput() ?>

            <?= $form->field($model, 'gift_keychain_amount')->textInput() ?>

            <?= $form->field($model, 'gift_poster_a3_amount')->textInput() ?>

            <?= $form->field($model, 'gift_poster_a4_amount')->textInput() ?>

            <?= $form->field($model, 'gift_mini_photobook_amount')->textInput() ?>

            <?= $form->field($model, 'gift_full_photobook_amount')->textInput() ?>

            <?= $form->field($model, 'gift_polo_shirt_amount')->textInput() ?>

            <?= $form->field($model, 'gift_certificate_amount')->textInput() ?>

            <?= $form->field($model, 'gift_akb_cd_amount')->textInput() ?>
        </div>
        <div class="col-md-4">
            <h1>ข้อมูลเสื้อโปโล</h1>
            <?= $form->field($model, 'gift_polo_shirt_size_s_amount')->textInput() ?>

            <?= $form->field($model, 'gift_polo_shirt_size_m_amount')->textInput() ?>

            <?= $form->field($model, 'gift_polo_shirt_size_l_amount')->textInput() ?>

            <?= $form->field($model, 'gift_polo_shirt_size_xl_amount')->textInput() ?>

            <?= $form->field($model, 'gift_polo_shirt_size_2xl_amount')->textInput() ?>

            <?= $form->field($model, 'gift_polo_shirt_size_3xl_amount')->textInput() ?>
        </div>
        <div class="col-md-4">
            <h1>จำนวนรูป</h1>
            <?= $form->field($model, 'gift_music_amount')->textInput()->hint('รูปเดี่ยว') ?>
            <hr/>

            <?= $form->field($model, 'gift_cherprang_amount')->textInput() ?>

            <?= $form->field($model, 'gift_tarwaan_amount')->textInput() ?>

            <?= $form->field($model, 'gift_jennis_amount')->textInput() ?>

            <?= $form->field($model, 'gift_pupe_amount')->textInput() ?>

            <?= $form->field($model, 'gift_noey_amount')->textInput() ?>

            <?= $form->field($model, 'gift_kate_amount')->textInput() ?>

            <?= $form->field($model, 'gift_jane_amount')->textInput() ?>

            <?= $form->field($model, 'gift_namneung_amount')->textInput() ?>

            <?= $form->field($model, 'gift_miori_amount')->textInput() ?>

            <?= $form->field($model, 'gift_jaa_amount')->textInput() ?>

            <?= $form->field($model, 'gift_kaew_amount')->textInput() ?>

            <?= $form->field($model, 'gift_can_amount')->textInput() ?>

            <?= $form->field($model, 'gift_mind_amount')->textInput() ?>

            <?= $form->field($model, 'gift_orn_amount')->textInput() ?>

            <?= $form->field($model, 'gift_namsai_amount')->textInput() ?>

            <?= $form->field($model, 'gift_mobile_amount')->textInput() ?>

            <?= $form->field($model, 'gift_pun_amount')->textInput() ?>

            <?= $form->field($model, 'gift_izurina_amount')->textInput() ?>

            <?= $form->field($model, 'gift_satchan_amount')->textInput() ?>

            <?= $form->field($model, 'gift_jib_amount')->textInput() ?>

            <?= $form->field($model, 'gift_korn_amount')->textInput() ?>

            <?= $form->field($model, 'gift_kaimook_amount')->textInput() ?>

            <?= $form->field($model, 'gift_nink_amount')->textInput() ?>

            <?= $form->field($model, 'gift_maysa_amount')->textInput() ?>

            <?= $form->field($model, 'gift_piam_amount')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success btn-block btn-lg']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
