<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReviewDonorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-donor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'review_id') ?>

    <?= $form->field($model, 'full_name') ?>

    <?= $form->field($model, 'address') ?>

    <?= $form->field($model, 'entry_date') ?>

    <?php // echo $form->field($model, 'is_required_gift') ?>

    <?php // echo $form->field($model, 'is_valid') ?>

    <?php // echo $form->field($model, 'take_gift') ?>

    <?php // echo $form->field($model, 'level') ?>

    <?php // echo $form->field($model, 'member_selection') ?>

    <?php // echo $form->field($model, 'picture_type') ?>

    <?php // echo $form->field($model, 'receiving_method') ?>

    <?php // echo $form->field($model, 'to_send_gift') ?>

    <?php // echo $form->field($model, 'gift_sent') ?>

    <?php // echo $form->field($model, 'mail_tracking_code') ?>

    <?php // echo $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'valid_total') ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'gift_wristband_number') ?>

    <?php // echo $form->field($model, 'gift_round_1_photo_amount') ?>

    <?php // echo $form->field($model, 'gift_picture_amount') ?>

    <?php // echo $form->field($model, 'gift_wristband_amount') ?>

    <?php // echo $form->field($model, 'gift_bag_amount') ?>

    <?php // echo $form->field($model, 'gift_necklace_amount') ?>

    <?php // echo $form->field($model, 'gift_keychain_amount') ?>

    <?php // echo $form->field($model, 'gift_poster_a3_amount') ?>

    <?php // echo $form->field($model, 'gift_poster_a4_amount') ?>

    <?php // echo $form->field($model, 'gift_mini_photobook_amount') ?>

    <?php // echo $form->field($model, 'gift_full_photobook_amount') ?>

    <?php // echo $form->field($model, 'gift_polo_shirt_amount') ?>

    <?php // echo $form->field($model, 'gift_polo_shirt_size_s_amount') ?>

    <?php // echo $form->field($model, 'gift_polo_shirt_size_m_amount') ?>

    <?php // echo $form->field($model, 'gift_polo_shirt_size_l_amount') ?>

    <?php // echo $form->field($model, 'gift_polo_shirt_size_xl_amount') ?>

    <?php // echo $form->field($model, 'gift_polo_shirt_size_2xl_amount') ?>

    <?php // echo $form->field($model, 'gift_polo_shirt_size_3xl_amount') ?>

    <?php // echo $form->field($model, 'gift_certificate_amount') ?>

    <?php // echo $form->field($model, 'gift_akb_cd_amount') ?>

    <?php // echo $form->field($model, 'gift_cherprang_amount') ?>

    <?php // echo $form->field($model, 'gift_tarwaan_amount') ?>

    <?php // echo $form->field($model, 'gift_jennis_amount') ?>

    <?php // echo $form->field($model, 'gift_pupe_amount') ?>

    <?php // echo $form->field($model, 'gift_noey_amount') ?>

    <?php // echo $form->field($model, 'gift_kate_amount') ?>

    <?php // echo $form->field($model, 'gift_jane_amount') ?>

    <?php // echo $form->field($model, 'gift_namneung_amount') ?>

    <?php // echo $form->field($model, 'gift_miori_amount') ?>

    <?php // echo $form->field($model, 'gift_jaa_amount') ?>

    <?php // echo $form->field($model, 'gift_kaew_amount') ?>

    <?php // echo $form->field($model, 'gift_can_amount') ?>

    <?php // echo $form->field($model, 'gift_mind_amount') ?>

    <?php // echo $form->field($model, 'gift_orn_amount') ?>

    <?php // echo $form->field($model, 'gift_namsai_amount') ?>

    <?php // echo $form->field($model, 'gift_mobile_amount') ?>

    <?php // echo $form->field($model, 'gift_music_amount') ?>

    <?php // echo $form->field($model, 'gift_pun_amount') ?>

    <?php // echo $form->field($model, 'gift_izurina_amount') ?>

    <?php // echo $form->field($model, 'gift_satchan_amount') ?>

    <?php // echo $form->field($model, 'gift_jib_amount') ?>

    <?php // echo $form->field($model, 'gift_korn_amount') ?>

    <?php // echo $form->field($model, 'gift_kaimook_amount') ?>

    <?php // echo $form->field($model, 'gift_nink_amount') ?>

    <?php // echo $form->field($model, 'gift_maysa_amount') ?>

    <?php // echo $form->field($model, 'gift_piam_amount') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
