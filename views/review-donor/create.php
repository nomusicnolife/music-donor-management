<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReviewDonor */

$this->title = 'เพิ่มข้อมูลสรุปผู้สนับสนุน';
$this->params['breadcrumbs'][] = ['label' => 'ผู้สนับสนุน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-donor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'reviewId' => $reviewId,
        'isReview' => $isReview
    ]) ?>

</div>
