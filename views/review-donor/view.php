<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ReviewDonor */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Review Donors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-donor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'review_id',
            'full_name',
            'address:ntext',
            'entry_date',
            'is_required_gift',
            'is_valid',
            'take_gift',
            'level',
            'member_selection',
            'picture_type',
            'receiving_method',
            'to_send_gift',
            'gift_sent',
            'mail_tracking_code',
            'total',
            'valid_total',
            'note:ntext',
            'gift_wristband_number',
            'gift_round_1_photo_amount',
            'gift_picture_amount',
            'gift_wristband_amount',
            'gift_bag_amount',
            'gift_necklace_amount',
            'gift_keychain_amount',
            'gift_poster_a3_amount',
            'gift_poster_a4_amount',
            'gift_mini_photobook_amount',
            'gift_full_photobook_amount',
            'gift_polo_shirt_amount',
            'gift_polo_shirt_size_s_amount',
            'gift_polo_shirt_size_m_amount',
            'gift_polo_shirt_size_l_amount',
            'gift_polo_shirt_size_xl_amount',
            'gift_polo_shirt_size_2xl_amount',
            'gift_polo_shirt_size_3xl_amount',
            'gift_certificate_amount',
            'gift_akb_cd_amount',
            'gift_cherprang_amount',
            'gift_tarwaan_amount',
            'gift_jennis_amount',
            'gift_pupe_amount',
            'gift_noey_amount',
            'gift_kate_amount',
            'gift_jane_amount',
            'gift_namneung_amount',
            'gift_miori_amount',
            'gift_jaa_amount',
            'gift_kaew_amount',
            'gift_can_amount',
            'gift_mind_amount',
            'gift_orn_amount',
            'gift_namsai_amount',
            'gift_mobile_amount',
            'gift_music_amount',
            'gift_pun_amount',
            'gift_izurina_amount',
            'gift_satchan_amount',
            'gift_jib_amount',
            'gift_korn_amount',
            'gift_kaimook_amount',
            'gift_nink_amount',
            'gift_maysa_amount',
            'gift_piam_amount',
        ],
    ]) ?>

</div>
