<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReviewTransaction */

$this->title = 'เพิ่มข้อมูลการโอนภายใต้ผลสรุป: ' . $model->reviewDonor->id_text;
$this->params['breadcrumbs'][] = ['label' => 'Review Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-transaction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
