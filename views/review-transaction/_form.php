<?php

use kartik\datecontrol\DateControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\ReviewDonor;

/* @var $this yii\web\View */
/* @var $model app\models\ReviewTransaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-transaction-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo $form->field($model, 'review_donor_id')
        ->widget(Select2::classname(),
            [
                'data' => ArrayHelper::map(
                    ReviewDonor::find()
                        ->andWhere([
                            'review_id' => $model->reviewDonor->review_id
                        ])
                        ->all(), 'id', 'id_text'),
                'size' => Select2::LARGE,
                'options' => [
                    'placeholder' => 'เลือกข้อมูลสรุป ...',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
    ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'method')
        ->widget(Select2::classname(),
            [
                'data' => [
                    1 => 'บัญชีธนาคาร (เก่า)',
                    2 => 'บัญชีธนาคาร (ใหม่)',
                    3 => 'True Wallet',
                    4 => 'Paypal',
                ],
                'options' => [
                    'placeholder' => 'เลือกวิธีการโอน ...',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
    ?>

    <?php
    echo $form->field($model, 'date')->widget(DateControl::classname(), [
        'type' => DateControl::FORMAT_DATETIME,
        'widgetOptions' => [
            'pluginOptions' => [
                'autoclose' => true
            ]
        ]
    ]);
    ?>

    <?php
    echo $form->field($model, 'valid')
        ->widget(Select2::classname(),
            [
                'data' => [
                    1 => 'โอนถูกต้อง',
                    0 => 'ไม่ถูกต้อง'
                ],
                'options' => ['placeholder' => 'การยืนยัน ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
    ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success btn-lg btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
