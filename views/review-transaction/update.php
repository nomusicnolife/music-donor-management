<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReviewTransaction */

$this->title = 'แก้ไขข้อมูลการโอนภายใต้ผลสรุป: ' . $model->reviewDonor->id_text;
$this->params['breadcrumbs'][] = ['label' => $model->reviewDonor->review->email, 'url' => ['review/view', 'id' => $model->reviewDonor->review->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-transaction-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'reviewDonorId' => $reviewDonorId
    ]) ?>

</div>
