<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Music 48 Project',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    $levels = \app\helpers\ReviewHelper::getLevels();
    unset($levels[1]);
    $levelsNav = [];
    foreach ($levels as $levelNumber => $levelText) {
        $levelsNav[] = [
            'label' => $levelText,
            'url' => ['review/address-label-summary', 'levelNumber' => $levelNumber]
        ];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'หน้าหลัก', 'url' => ['/site/index']],
            ['label' => 'รีวิว', 'url' => ['/review']],
            ['label' => 'ข้อมูลสรุปผู้สนับสนุน', 'url' => ['/review-donor']],
            ['label' => 'ภาพรวมการรีวิว', 'url' => ['/review/summary']],
            ['label' => 'สรุปของที่ระลึก', 'url' => ['/review/gift-summary']],
            ['label' => 'สรุปผู้รับเอง', 'url' => ['/review/self-receiving']],
            [
                'label' => 'สรุปที่อยู่',
                'items' => $levelsNav,
            ],
            ['label' => 'สรุปที่ค้างส่ง', 'url' => ['/review/gift-sending-summary']],
            Yii::$app->user->isGuest ? (
            ['label' => 'เข้าระบบ', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'ออกจากระบบ (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Music 48 Project <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
