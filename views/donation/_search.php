<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DonationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="donation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'timestamp') ?>

    <?= $form->field($model, 'donate_confirm') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'donate_time') ?>

    <?php // echo $form->field($model, 'is_continue') ?>

    <?php // echo $form->field($model, 'channel') ?>

    <?php // echo $form->field($model, 'slip_url') ?>

    <?php // echo $form->field($model, 'full_name') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'take_gift') ?>

    <?php // echo $form->field($model, 'receiving_method') ?>

    <?php // echo $form->field($model, 'picture_type') ?>

    <?php // echo $form->field($model, 'member_selection') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'facebook_id') ?>

    <?php // echo $form->field($model, 'line_id') ?>

    <?php // echo $form->field($model, 'twitter_id') ?>

    <?php // echo $form->field($model, 'name_signing') ?>

    <?php // echo $form->field($model, 'alias_name') ?>

    <?php // echo $form->field($model, 'message') ?>

    <?php // echo $form->field($model, 'note') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
