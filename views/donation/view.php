<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Donation */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Donations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'timestamp',
            'donate_confirm',
            'email:email',
            'amount',
            'donate_time',
            'is_continue',
            'channel',
            'slip_url:url',
            'full_name',
            'address:ntext',
            'take_gift',
            'receiving_method',
            'picture_type',
            'member_selection',
            'phone',
            'facebook_id',
            'line_id',
            'twitter_id',
            'name_signing',
            'alias_name',
            'message:ntext',
            'note:ntext',
        ],
    ]) ?>

</div>
