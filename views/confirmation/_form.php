<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Confirmation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="confirmation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'confirmation_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hash')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'confirm_time')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
