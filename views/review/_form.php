<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Review */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'confirmation_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hash')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'confirmation_time')->textInput() ?>

    <?= $form->field($model, 'review_process')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reason_is_single')->textInput() ?>

    <?= $form->field($model, 'reason_is_confirm')->textInput() ?>

    <?= $form->field($model, 'reason_is_data_confirm')->textInput() ?>

    <?= $form->field($model, 'reason_is_all_valid')->textInput() ?>

    <?= $form->field($model, 'reason_is_all_similar_address')->textInput() ?>

    <?= $form->field($model, 'reason_is_all_similar_member_selecting')->textInput() ?>

    <?= $form->field($model, 'round_number')->textInput() ?>

    <?= $form->field($model, 'all_notes')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'review_class')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'review_time')->textInput() ?>

    <?= $form->field($model, 'is_review')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
