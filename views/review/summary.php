<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;

$this->title = 'ภาพรวมการรีวิว';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <h1><?php echo Html::encode($this->title) ?></h1>
    </div>
</div>
<?php
$review = new \app\models\Review();
?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>กลุ่ม</th>
                <?php
                foreach ($conditions as $condition) {
                    echo '<th>' . str_replace('กลุ่ม : ', '', $review->getAttributeLabel($condition)) . '</th>';
                }
                ?>
                <th>จำนวนที่รีวิวแล้ว</th>
                <th>ยังค้างรีวิว</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $rowNumber = 1;
            foreach ($summary as $class => $row) {
                echo '<tr>';
                echo '<td>' . $rowNumber++ . '</td>';
                echo '<td>' . $class . '</td>';
                foreach ($conditions as $condition) {
                    if (isset($row[$condition])) {
                        $value = $row[$condition];
                        $label = 'g';
                        if ($value === 1) {
                            $label = 'glyphicon glyphicon-ok text-success';
                        } else if ($value === 0) {
                            $label = 'glyphicon glyphicon-remove text-danger';
                        }
                        echo '<td><span class="' . $label . '"></span></td>';
                    }
                }
                echo '<td>' . $row['review'] . '</td>';
                echo '<td>' . $row['not_review'] . '</td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
</div>