<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Review */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => 'รีวิว', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-view">

    <h1>ข้อมูลการรีวิว : <?= Html::encode($this->title) ?></h1>
    <p>
        <?php
        if ($model->is_review != 1) {
            echo Html::a('ยืนยันการรีวิว', ['review', 'id' => $model->id, 'review' => 1], [
                'class' => 'btn btn-lg btn-success btn-block'
            ]);
        }
        ?>
        <?php
        //        echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])
        ?>
        <?php
        //        echo Html::a('Delete', ['delete', 'id' => $model->id], [
        //            'class' => 'btn btn-danger',
        //            'data' => [
        //                'confirm' => 'Are you sure you want to delete this item?',
        //                'method' => 'post',
        //            ],
        //        ]);
        ?>
    </p>

    <div class="row">
        <div class="col-md-2 text-center">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">การรีวิว</h3>
                </div>
                <div class="panel-body">
                    <?php echo $this->render('_reason_box', [
                        'reason' => $model->is_review,
                        'reasonText' => $model->review_time
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-md-2 text-center">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">กลุ่ม</h3>
                </div>
                <div class="panel-body">
                    <span style="font-size: 60pt; font-weight: bold;">
                        <?php echo $model->review_class; ?>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-2 text-center">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">รอบ</h3>
                </div>
                <div class="panel-body">
                    <span style="font-size: 60pt; font-weight: bold;">
                    <?php echo $model->round_number; ?>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-6 text-center">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">หมายเหตุ</h3>
                </div>
                <div class="panel-body">
                    <?php echo $model->all_notes; ?>
                </div>
            </div>
        </div>
        <div class="col-md-4 text-center">

        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">การกรองข้อมูล</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2 text-center">
                    <?php echo $this->render('_reason_box', [
                        'reason' => $model->reason_is_single,
                        'reasonText' => 'โอนครั้งเดียว'
                    ]); ?>
                </div>
                <div class="col-md-2 text-center">
                    <?php echo $this->render('_reason_box', [
                        'reason' => $model->reason_is_confirm,
                        'reasonText' => 'ยืนยันแล้ว'
                    ]); ?>
                </div>
                <div class="col-md-2 text-center">
                    <?php echo $this->render('_reason_box', [
                        'reason' => $model->reason_is_data_confirm,
                        'reasonText' => 'บันทึกข้อมูลยืนยัน'
                    ]); ?>
                </div>
                <div class="col-md-2 text-center">
                    <?php echo $this->render('_reason_box', [
                        'reason' => $model->reason_is_all_valid,
                        'reasonText' => 'โอนถูกต้องทุกครั้ง'
                    ]); ?>
                </div>
                <div class="col-md-2 text-center">
                    <?php echo $this->render('_reason_box', [
                        'reason' => $model->reason_is_all_similar_address,
                        'reasonText' => 'กรอกที่อยู่เหมือนกัน'
                    ]); ?>
                </div>
                <div class="col-md-2 text-center">
                    <?php echo $this->render('_reason_box', [
                        'reason' => $model->reason_is_all_similar_member_selecting,
                        'reasonText' => 'เลือกรูปเหมือนกัน'
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

    <h1>ข้อมูลการสนับสนุน</h1>
    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $donationProvider,
            'tableOptions' => ['class' => 'table table-condensed table-striped table-bordered'],
            'columns' => [
                'timestamp',
                'donate_confirm:boolean',
                'email',
                'amount',
                'donate_time',
                'is_continue',
                'channel',
                'slip_url:url',
                'full_name',
                'address:ntext',
                'take_gift',
                'receiving_method',
                'picture_type',
                'member_selection',
                'note:ntext',
            ],
        ]) ?>
    </div>

    <h1>ผลสรุป</h1>
    <p>
        <?= Html::a('เพิ่มผลสรุป',
            [
                'review-donor/create',
                'reviewId' => $model->id,
                'isReview' => 1
            ],
            ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    echo ListView::widget([
        'dataProvider' => $reviewDonorProvider,
        'itemView' => '_review_donor',
        'viewParams' => [
            'email' => $model->email,
            'class' => $model->review_class
        ]
    ]);
    ?>

    <h1>ข้อมูลอื่นๆ ของระบบ</h1>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">ข้อมูลอื่นๆ</h3>
        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'confirmation_key',
                    'hash:ntext',
                    'confirmation_time',
                    'review_process',
                ],
            ]); ?>
        </div>
    </div>

    <?php
    if ($model->is_review != 1) {
        echo Html::a('ยืนยันการรีวิว', ['review', 'id' => $model->id, 'review' => 1], [
            'class' => 'btn btn-lg btn-success btn-block'
        ]);
    }
    ?>
</div>
