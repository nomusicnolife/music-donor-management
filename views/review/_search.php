<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReviewSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'confirmation_key') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'hash') ?>

    <?= $form->field($model, 'confirmation_time') ?>

    <?php // echo $form->field($model, 'review_process') ?>

    <?php // echo $form->field($model, 'reason_is_single') ?>

    <?php // echo $form->field($model, 'reason_is_confirm') ?>

    <?php // echo $form->field($model, 'reason_is_data_confirm') ?>

    <?php // echo $form->field($model, 'reason_is_all_valid') ?>

    <?php // echo $form->field($model, 'reason_is_all_similar_address') ?>

    <?php // echo $form->field($model, 'reason_is_all_similar_member_selecting') ?>

    <?php // echo $form->field($model, 'round_number') ?>

    <?php // echo $form->field($model, 'all_notes') ?>

    <?php // echo $form->field($model, 'review_class') ?>

    <?php // echo $form->field($model, 'review_time') ?>

    <?php // echo $form->field($model, 'is_review') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
