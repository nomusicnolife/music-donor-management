<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รีวิว';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //echo Html::a('Create Review', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => ''
        ],
        'autoXlFormat' => true,
        'toolbar' => [
            '{export}',
            '{toggleData}',
        ],
        'export' => [
            'fontAwesome' => true,
            'showConfirmAlert' => false,
            'target' => GridView::TARGET_BLANK
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => 'รายการรีวิว',
        ],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'email',
            'review_class',
            'round_number',
            [
                'attribute' => 'has_notes',
                'label' => 'มีหมายเหตุ',
                'filter' => [
                    1 => 'มีหมายเหตุ',
                    0 => 'ไม่มีหมายเหตุ'
                ],
                'value' => function ($model) {
                    $label = 'glyphicon glyphicon-minus text-muted';
                    if (!empty($model->all_notes)) {
                        $label = 'glyphicon glyphicon-ok text-primary';
                    }
                    return '<span class="' . $label . '"></span>';
                },
                'format' => 'html'
            ],
            'all_notes:ntext',
            'review_time',
            [
                'attribute' => 'is_review',
                'filter' => [
                    1 => 'รีวิวแล้ว',
                    0 => 'ยังไม่ได้รีวิว'
                ],
                'value' => function ($model) {
                    $label = 'glyphicon glyphicon-minus text-muted';
                    if ($model->is_review == 1) {
                        $label = 'glyphicon glyphicon-ok text-success';
                    } else if ($model->is_review == 0) {
                        $label = 'glyphicon glyphicon-remove text-danger';
                    }
                    return '<span class="' . $label . '"></span>';
                },
                'format' => 'html'
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view}'

            ],
        ],
    ]); ?>
</div>
