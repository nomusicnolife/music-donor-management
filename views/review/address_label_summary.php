<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;

$this->title = 'สรุปที่อยู่: ' . $levelText . ' (' . count($result) . ' รายการ)';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <h1><?php echo Html::encode($this->title) ?></h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php echo
        Html::a(
            'ดาวน์โหลด' . $this->title,
            ['data/download-address-label-excel', 'levelNumber' => $levelNumber],
            ['class' => 'btn btn-success btn-lg']) ?>
        <br/>
        <br/>
        <p>
            เฉพาะรายการที่ผ่านการรีวิวแล้ว และต้องการรับของที่ระลึกผ่านทางไปรษณีย์
        </p>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>รหัสอ้างอิง</th>
                <th>Email</th>
                <th>ชื่อ</th>
                <th>ที่อยู่</th>
                <th>โทร</th>
                <th>ระดับ</th>
                <th>กรณี</th>
                <th>ของที่ระลึก</th>
            </tr>
            </thead>
            <tbody>
            <?php

            foreach ($result as $address) {
                echo '<tr>';
                echo '<td>';
                echo $address['รหัสอ้างอิง'];
                echo '</td>';
                echo '<td>';
                echo $address['email'];
                echo '</td>';
                echo '<td>';
                echo $address['ชื่อ'];
                echo '</td>';
                echo '<td>';
                echo $address['ที่อยู่'];
                echo '</td>';
                echo '<td>';
                echo $address['โทร'];
                echo '</td>';
                echo '<td>';
                echo $address['ระดับ'];
                echo '</td>';
                echo '<td>';
                echo $address['กรณี'];
                echo '</td>';
                echo '<td>';
                $gifts = $address['ของที่ระลึก'];
                echo implode('<br />', $gifts);
                echo '</td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
</div>