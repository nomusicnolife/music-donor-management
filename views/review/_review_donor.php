<?php
/* @var \app\models\ReviewDonor $model */

use app\models\Review;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\DetailView;
use Stringy\Stringy as S;

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3>ข้อมูลสรุปผู้สนับสนุน: <span class="label label-primary"><?php echo $email . '#' . ($model->id); ?></span>
        </h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-8">
                <h3>ข้อมูลหลัก</h3>
                <?php
                echo DetailView::widget([
                    'model' => $model,
                    'template' => '<tr><th{captionOptions} class="col-md-4">{label}</th><td{contentOptions}>{value}</td></tr>',
                    'attributes' => [
                        'full_name',
                        'address',
                        [
                            'attribute' => 'receiving_method_text',
                            'value' => function ($model) {
                                $label = 'glyphicon glyphicon-minus text-muted';
                                if ($model->receiving_method === Review::RECEIVING_METHOD_SELF) {
                                    $label = 'glyphicon glyphicon-user text-primary';
                                } else if ($model->receiving_method === Review::RECEIVING_METHOD_MAIL) {
                                    $label = 'glyphicon glyphicon-envelope text-primary';
                                }
                                return '<span class="' . $label . '"> ' . $model->receiving_method_text . ' </span>';
                            },
                            'format' => 'html'
                        ],
                        'entry_date:date',
                        [
                            'attribute' => 'is_required_gift',
                            'value' => function ($model) {
                                $label = 'glyphicon glyphicon-minus text-muted';
                                if ($model->is_required_gift === 1) {
                                    $label = 'glyphicon glyphicon-ok text-success';
                                } else if ($model->is_required_gift === 0) {
                                    $label = 'glyphicon glyphicon-remove text-danger';
                                }
                                return '<span class="' . $label . '"> ' . $model->is_required_gift_text . ' </span>';
                            },
                            'format' => 'html'
                        ],
                        [
                            'attribute' => 'take_gift_text',
                            'value' => function ($model) {
                                $label = 'glyphicon glyphicon-minus text-muted';
                                if ($model->take_gift === Review::TAKE_GIFT_YES) {
                                    $label = 'glyphicon glyphicon-ok text-success';
                                } else if ($model->take_gift === Review::TAKE_GIFT_NO) {
                                    $label = 'glyphicon glyphicon-remove text-danger';
                                }
                                return '<span class="' . $label . '"> ' . $model->take_gift_text . ' </span>';
                            },
                            'format' => 'html'
                        ],
                        [
                            'attribute' => 'to_send_gift_text',
                            'value' => function ($model) {
                                $label = 'glyphicon glyphicon-minus text-muted';
                                if ($model->to_send_gift == 1) {
                                    $label = 'glyphicon glyphicon-ok text-success';
                                } else if ($model->to_send_gift == 0) {
                                    $label = 'glyphicon glyphicon-remove text-danger';
                                }
                                return '<span class="' . $label . '"> ' . $model->to_send_gift_text . ' </span>';
                            },
                            'format' => 'html'
                        ],
                        'level_text',
                        [
                            'attribute' => 'picture_type_text',
                            'value' => function ($model) {
                                $label = 'glyphicon glyphicon-minus text-muted';
                                if ($model->picture_type == Review::PICTURE_TYPE_SOLO) {
                                    $label = 'glyphicon glyphicon-user text-primary';
                                } else if ($model->picture_type == Review::PICTURE_TYPE_DUO) {
                                    $label = 'glyphicon glyphicon-star text-primary';
                                }
                                return '<span class="' . $label . '"> ' . $model->picture_type_text . ' </span>';
                            },
                            'format' => 'html'
                        ],
                        'valid_total',
                        'note'
                    ],
                ])
                ?>
            </div>
            <div class="col-md-4">
                <h3>ของที่ระลึก</h3>
                <?php
                $giftsList = [];
                $attributes = $model->attributes;
                foreach ($attributes as $attributeName => $value) {
                    $attributeText = S::create($attributeName);
                    if ($attributeText->startsWith('gift')
                        && !$attributeText->containsAny([
                            'gift_sent',
                            'gift_picture_amount',
                            'gift_polo_shirt_size_s_amount',
                            'gift_polo_shirt_size_m_amount',
                            'gift_polo_shirt_size_l_amount',
                            'gift_polo_shirt_size_x_amount',
                            'gift_polo_shirt_size_xl_amount',
                            'gift_polo_shirt_size_2xl_amount',
                            'gift_polo_shirt_size_3xl_amount',
                        ])) {
                        $label = S::create($model->getAttributeLabel($attributeName));
                        $giftsList[] = [
                            'label' => $label->removeLeft('จำนวน'),
                            'attribute' => $attributeName,
                            'visible' => $value > 0
                        ];
                    }
                }

                echo DetailView::widget([
                    'model' => $model,
                    'template' => '<tr><th{captionOptions} class="col-md-10">{label}</th><td{contentOptions}>{value}</td></tr>',
                    'attributes' => $giftsList
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3>ข้อมูลการโอนที่อยู่ภายใต้ผลสรุปนี้</h3>
                <p>
                    <?php echo Html::a('เพิ่มข้อมูลการโอน',
                        [
                            'review-transaction/create',
                            'reviewDonorId' => $model->id,
                        ],
                        ['class' => 'btn btn-success']) ?>
                </p>
                <?php
                $dataProvider = new ActiveDataProvider([
                    'query' => \app\models\ReviewTransaction::find()
                        ->andWhere([
                            'review_donor_id' => $model->id
                        ]),
                    'pagination' => false
                ]);
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'tableOptions' => ['class' => 'table table-condensed table-striped table-bordered'],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'value',
                        'method_text',
                        'date',
                        [
                            'attribute' => 'valid',
                            'value' => function ($model) {
                                $label = 'glyphicon glyphicon-minus text-muted';
                                if ($model->valid == 1) {
                                    $label = 'glyphicon glyphicon-ok text-success';
                                } else if ($model->valid == 0) {
                                    $label = 'glyphicon glyphicon-remove text-danger';
                                }
                                return '<span class="' . $label . '"></span>';
                            },
                            'format' => 'html'
                        ],
                        'comment',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}',
                            'urlCreator' => function ($action, $model, $key, $index, $column) {
                                return Url::toRoute([
                                    'review-transaction/' . $action,
                                    'id' => (string)$key,
                                    'reviewDonorId' => $model->review_donor_id
                                ]);
                            }
                        ]
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php
                if ($class == 'C' || $class == 'D') {
                    echo '<h2>รายชื่อคล้ายคลึงในกลุ่ม I, J, K, L</h2>';
                    $fullName = S::create($model->full_name);
                    $names = $fullName->split(' ');
                    if (isset($names[0])) {
                        $firstName = $names[0]->replace('นาย', '');
                        $firstName = $firstName->replace('นาง', '');
                        $firstName = $firstName->replace('นางสาว', '');
                        $firstName = $firstName->replace('.', '');

                        echo '<h5>ค้นหาชื่อ "' . $firstName . '" ในกลุ่ม I, J, K, L...</h5>';

                        $reviewDonors = \app\models\ReviewDonor::find()
                            ->andWhere([
                                'like', 'full_name', (string)$firstName
                            ])
                            ->andWhere([
                                '!=', 'id', $model->id
                            ])
                            ->asArray()
                            ->all();

                        $reviews = Review::find()
                            ->andWhere([
                                'id' => \yii\helpers\ArrayHelper::getColumn($reviewDonors, 'review_id'),
                                'review_class' => [
                                    'I', 'J', 'K', 'L'
                                ]
                            ])
                            ->asArray()
                            ->all();

                        if (count($reviews) > 0) {
                            $donations = \app\models\Donation::find()
                                ->andWhere([
                                    'email' => \yii\helpers\ArrayHelper::getColumn($reviews, 'email')
                                ])
                                ->asArray()
                                ->all();

                            echo GridView::widget([
                                'dataProvider' => new ArrayDataProvider([
                                    'allModels' => $donations,
                                ]),
                                'tableOptions' => ['class' => 'table table-condensed table-striped table-bordered'],
                                'columns' => [
                                    'timestamp',
                                    'donate_confirm:boolean',
                                    'email',
                                    'amount',
                                    'donate_time',
                                    'is_continue',
                                    'channel',
                                    'slip_url:url',
                                    'full_name',
                                    'address:ntext',
                                    'take_gift',
                                    'receiving_method',
                                    'picture_type',
                                    'member_selection',
                                    'note:ntext',
                                ],
                            ]);
                        } else {
                            echo 'ไม่มีรายชื่อคล้ายคลึงในกลุ่ม I, J, K, L';
                        }
                    } else {
                        echo 'ข้อมูลชื่อไม่ถูกต้อง';
                    }
                }

                ?>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span class="pull-right">
                    <?php echo Html::a('แก้ไขผลสรุป',
                        [
                            'review-donor/update',
                            'id' => $model->id,
                            'isReview' => 1
                        ],
                        ['class' => 'btn btn-primary btn-lg']) ?>
                    <?php echo Html::a('ลบผลสรุป',
                        [
                            'review-donor/delete',
                            'id' => $model->id,
                            'isReview' => 1
                        ],
                        ['class' => 'btn btn-danger btn-lg',
                            'data' => ['confirm' => 'ต้องการลบผลสรุปนี้?',
                                'method' => 'post',],]) ?>
                </span>
            </div>
        </div>

    </div>
</div>