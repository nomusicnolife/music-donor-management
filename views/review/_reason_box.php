<h1>
    <?php
    $label = 'glyphicon glyphicon-minus text-muted';
    if ($reason === 1) {
        $label = 'glyphicon glyphicon-ok text-success';
    } else if ($reason === 0) {
        $label = 'glyphicon glyphicon-remove text-danger';
    }
    echo '<span class="' . $label . '"></span>';
    ?>
</h1>
<h4><?php echo $reasonText ?></h4>