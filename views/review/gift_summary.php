<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;

$this->title = 'สรุปของที่ระลึก';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <h1><?php echo Html::encode($this->title) ?></h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php echo
        Html::a(
            'ดาวน์โหลดสรุปของที่ระลึก',
            ['data/download-gift-summary-excel'],
            ['class' => 'btn btn-success btn-lg']) ?>
        <br />
        <br />
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>ของที่ระลึก</th>
                <th>จำนวน</th>
            </tr>
            </thead>
            <tbody>
            <?php

            foreach ($result as $name => $value) {
                echo '<tr>';
                echo '<td>';
                echo $name;
                echo '</td>';
                echo '<td>';
                echo $value;
                echo '</td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
</div>