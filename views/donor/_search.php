<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DonorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="donor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'full_name') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'address') ?>

    <?= $form->field($model, 'entry_date') ?>

    <?php // echo $form->field($model, 'hash') ?>

    <?php // echo $form->field($model, 'is_required_gift') ?>

    <?php // echo $form->field($model, 'is_valid') ?>

    <?php // echo $form->field($model, 'level') ?>

    <?php // echo $form->field($model, 'member_selection') ?>

    <?php // echo $form->field($model, 'picture_type') ?>

    <?php // echo $form->field($model, 'receiving_method') ?>

    <?php // echo $form->field($model, 'take_gift') ?>

    <?php // echo $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'valid_total') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
