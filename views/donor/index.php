<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DonorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Donors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Donor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'full_name',
            'email:email',
            'address:ntext',
            'entry_date',
            //'hash',
            //'is_required_gift',
            //'is_valid',
            //'level',
            //'member_selection',
            //'picture_type',
            //'receiving_method',
            //'take_gift',
            //'total',
            //'valid_total',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
