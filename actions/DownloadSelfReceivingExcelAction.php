<?php

namespace app\actions;

use app\helpers\ReviewHelper;
use app\models\Review;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use yii\base\Action;

class DownloadSelfReceivingExcelAction extends Action
{
    public function run()
    {
        ini_set('memory_limit', '2048M');

        $donors = ReviewHelper::getAddressLabel(null, Review::RECEIVING_METHOD_SELF);

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Music 48 Project')
            ->setLastModifiedBy('Music 48 Project')
            ->setTitle('Music Donor Address Label')
            ->setSubject('Music Donor Address Label')
            ->setDescription('Music Donor Address Label')
            ->setKeywords('music donor address label')
            ->setCategory('Export');

        $spreadsheet->setActiveSheetIndex(0);

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setTitle('Address');

        $sheet->setCellValue('A1', 'รหัสอ้างอิง');
        $sheet->setCellValue('B1', 'email');
        $sheet->setCellValue('C1', 'ชื่อ');
        $sheet->setCellValue('D1', 'ระดับ');
        $sheet->setCellValue('E1', 'ของที่ระลึก');
        $sheet->setCellValue('F1', 'โทร');
        $sheet->setCellValue('G1', 'กรณี');
        $sheet->setCellValue('H1', 'รับแล้ว');

        $rowIndex = 2;
        foreach ($donors as $donor) {
            $cell1 = $sheet->getCellByColumnAndRow(1, $rowIndex);
            $cell1->setValue($donor['รหัสอ้างอิง']);

            $cell2 = $sheet->getCellByColumnAndRow(2, $rowIndex);
            $cell2->setValue($donor['email']);

            $cell3 = $sheet->getCellByColumnAndRow(3, $rowIndex);
            $cell3->setValue($donor['ชื่อ']);

            $cell4 = $sheet->getCellByColumnAndRow(4, $rowIndex);
            $cell4->setValue($donor['ระดับ']);

            $cell5 = $sheet->getCellByColumnAndRow(5, $rowIndex);
            $gifts = $donor['ของที่ระลึก'];
            $cell5->setValue(implode(' ', $gifts));

            $cell6 = $sheet->getCellByColumnAndRow(6, $rowIndex);
            $cell6->setValue($donor['โทร']);

            $cell7 = $sheet->getCellByColumnAndRow(7, $rowIndex);
            $cell7->setValue($donor['กรณี']);

            $cell8 = $sheet->getCellByColumnAndRow(8, $rowIndex);
            $cell8->setValue($donor['ส่งหรือรับของที่ระลึกแล้ว']);

            $rowIndex++;
        }

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="music_donor_self_receiving.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
}