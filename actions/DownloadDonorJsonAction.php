<?php

namespace app\actions;

use app\models\ReviewDonor;
use yii\base\Action;

class DownloadDonorJsonAction extends Action
{
    public function run()
    {
        ini_set('memory_limit', '2048M');

        $donors = ReviewDonor::find()
            ->all();

        $giftLabels = [
            'gift_round_1_photo_amount',
            'gift_polo_shirt_size_s_amount',
            'gift_polo_shirt_size_m_amount',
            'gift_polo_shirt_size_l_amount',
            'gift_polo_shirt_size_xl_amount',
            'gift_polo_shirt_size_2xl_amount',
            'gift_polo_shirt_size_3xl_amount',
            'gift_wristband_amount',
            'gift_bag_amount',
            'gift_necklace_amount',
            'gift_keychain_amount',
            'gift_poster_a3_amount',
            'gift_poster_a4_amount',
            'gift_mini_photobook_amount',
            'gift_full_photobook_amount',
            'gift_certificate_amount',
            'gift_akb_cd_amount',
            'gift_cherprang_amount',
            'gift_tarwaan_amount',
            'gift_jennis_amount',
            'gift_pupe_amount',
            'gift_noey_amount',
            'gift_kate_amount',
            'gift_jane_amount',
            'gift_namneung_amount',
            'gift_miori_amount',
            'gift_jaa_amount',
            'gift_kaew_amount',
            'gift_can_amount',
            'gift_mind_amount',
            'gift_orn_amount',
            'gift_namsai_amount',
            'gift_mobile_amount',
            'gift_music_amount',
            'gift_pun_amount',
            'gift_izurina_amount',
            'gift_satchan_amount',
            'gift_jib_amount',
            'gift_korn_amount',
            'gift_kaimook_amount',
            'gift_nink_amount',
            'gift_maysa_amount',
            'gift_piam_amount',
        ];

        $jsonData = [];
        foreach ($donors as $donor) {
            $transactions = $donor->reviewTransactions;
            $donates = [];
            foreach ($transactions as $transaction) {
                $donates[] = (object)[
                    'comment' => $transaction->comment,
                    'date' => isset($transaction->date) ? $transaction->date : '',
                    'method' => $transaction->method_text,
                    'valid' => $transaction->valid,
                    'value' => $transaction->value,
                ];
            }

            $gifts = [];
            foreach ($giftLabels as $giftLabel) {
                $value = intval($donor->getAttribute($giftLabel));
                if ($value > 0) {
                    $gifts[$donor->getAttributeLabel($giftLabel)] = $value;
                }
            }

            $jsonData[] = (object)[
                'address' => $donor->address,
                'donate' => $donates,
                'email' => $donor->review->email,
                'entryDate' => $donor->entry_date,
                'hash' => $donor->review->hash,
                'isConfirm' => !empty($donor->review->confirmation_key) ? 1 : 0,
                'isRequiredGift' => $donor->is_required_gift,
                'isValid' => $donor->is_valid,
                'level' => $donor->level_text,
                'member' => $donor->member_selection,
                'name' => $donor->full_name,
                'photoType' => $donor->picture_type_text,
                'receivingMethod' => $donor->receiving_method_text,
                'takeGift' => $donor->take_gift_text,
                'toSendGift' => $donor->to_send_gift,
                'total' => $donor->total,
                'validTotal' => $donor->valid_total,
                'isGiftSent' => $donor->gift_sent != null ? $donor->gift_sent : 0,
                'trackingCode' => $donor->mail_tracking_code != null ? $donor->mail_tracking_code : '',
                'giftList' => (object) $gifts
            ];
        }

        // Redirect output to a client’s web browser (Ods)
        header("Content-type: text/plain");
        header("Content-Disposition: attachment; filename=donors-data.json");
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        echo json_encode($jsonData);
        exit;
    }
}