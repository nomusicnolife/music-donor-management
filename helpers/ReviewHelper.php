<?php

namespace app\helpers;

use app\models\Confirmation;
use app\models\ConfirmationDonor;
use app\models\Donation;
use app\models\Donor;
use app\models\Review;
use app\models\ReviewDonor;
use app\models\ReviewTransaction;
use yii\db\Expression;
use Stringy\Stringy as S;
use yii\helpers\ArrayHelper;

class ReviewHelper
{
    public static function getAllClasses()
    {
        return [
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L'
        ];
    }

    public static function getClassCondition($class)
    {
        /*
 *
A : reason_is_single = 1 AND reason_is_confirm = 1 AND reason_is_data_confirm = 1
B : reason_is_single = 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1
C : reason_is_single = 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1
D : reason_is_single = 1 AND reason_is_confirm != 1 AND reason_is_all_valid != 1
E : reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm = 1
F : reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting = 1
G : reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting != 1
H : reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1 AND reason_is_all_similar_address != 1
I : reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting = 1
J : reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting != 1
K : reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1 AND reason_is_all_similar_address != 1
L : reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid != 1
 */

        $condition = '';
        switch ($class) {
            case 'A':
                $condition = 'reason_is_single = 1 AND reason_is_confirm = 1 AND reason_is_data_confirm = 1';
                break;
            case 'B':
                $condition = 'reason_is_single = 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1';
                break;
            case 'C':
                $condition = 'reason_is_single = 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1';
                break;
            case 'D':
                $condition = 'reason_is_single = 1 AND reason_is_confirm != 1 AND reason_is_all_valid != 1';
                break;
            case 'E':
                $condition = 'reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm = 1';
                break;
            case 'F':
                $condition = 'reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting = 1';
                break;
            case 'G':
                $condition = 'reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting != 1';
                break;
            case 'H':
                $condition = 'reason_is_single != 1 AND reason_is_confirm = 1 AND reason_is_data_confirm != 1 AND reason_is_all_similar_address != 1';
                break;
            case 'I':
                $condition = 'reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting = 1';
                break;
            case 'J':
                $condition = 'reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1 AND reason_is_all_similar_address = 1 AND reason_is_all_similar_member_selecting != 1';
                break;
            case 'K':
                $condition = 'reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid = 1 AND reason_is_all_similar_address != 1';
                break;
            case 'L':
                $condition = 'reason_is_single != 1 AND reason_is_confirm != 1 AND reason_is_all_valid != 1';
                break;
        }
        return $condition;
    }

    public static function getLevels()
    {
        return [
            1 => 'Music Basic',
            2 => 'Music Backpacker',
            3 => 'Music Fan',
            4 => 'Music Cheerleader',
            5 => 'Music Ranger',
            6 => 'Music Ace Fan',
            7 => 'Music Veteran',
            8 => 'Music Champion',
        ];
    }

    public static function getLevelText($levelNumber)
    {
        switch ($levelNumber) {
            case 1:
                $levelText = 'Music Basic';
                break;
            case 2:
                $levelText = 'Music Backpacker';
                break;
            case 3:
                $levelText = 'Music Fan';
                break;
            case 4:
                $levelText = 'Music Cheerleader';
                break;
            case 5:
                $levelText = 'Music Ranger';
                break;
            case 6:
                $levelText = 'Music Ace Fan';
                break;
            case 7:
                $levelText = 'Music Veteran';
                break;
            case 8:
                $levelText = 'Music Champion';
                break;
            default:
                $levelText = 'ระบุไม่ได้';
        }
        return $levelText;
    }

    public static function getDuoMembers()
    {
        return [
            'Cherprang' => 'Cherprang',
            'Tarwaan' => 'Tarwaan',
            'Jennis' => 'Jennis',
            'Pupe' => 'Pupe',
            'Noey' => 'Noey',
            'Kate' => 'Kate',
            'Jane' => 'Jane',
            'Namneung' => 'Namneung',
            'Miori' => 'Miori',
            'Jaa' => 'Jaa',
            'Kaew' => 'Kaew',
            'Can' => 'Can',
            'Mind' => 'Mind',
            'Orn' => 'Orn',
            'Namsai' => 'Namsai',
            'Mobile' => 'Mobile',
            'Pun' => 'Pun',
            'Izurina' => 'Izurina',
            'Satchan' => 'Satchan',
            'Jib' => 'Jib',
            'Korn' => 'Korn',
            'Kaimook' => 'Kaimook',
            'Nink' => 'Nink',
            'Maysa' => 'Maysa',
            'Piam' => 'Piam',
        ];
    }

    public static function createReviewDonor(Review $review, $donor, $use)
    {
        $newComments = [];

        $reviewDonor = new ReviewDonor();
        $reviewDonor->review_id = $review->id;
        $reviewDonor->full_name = $donor->full_name;
        $reviewDonor->address = $donor->address;
        $reviewDonor->entry_date = $donor->entry_date;
        $reviewDonor->is_required_gift = $donor->is_required_gift;
        // $donor->is_valid is no longer used
        $reviewDonor->is_valid = 0;
        $reviewDonor->level = $donor->level;
        $reviewDonor->member_selection = $donor->member_selection;
        $reviewDonor->picture_type = $donor->picture_type;
        $reviewDonor->take_gift = $donor->take_gift;
        $reviewDonor->total = $donor->total;
        $reviewDonor->valid_total = $donor->valid_total;
        $reviewDonor->receiving_method = $donor->receiving_method;

        if ($donor->receiving_method == Review::RECEIVING_METHOD_SELF) {
            $reviewDonor->gift_poster_a4_amount = 1;
        }

        if ($donor->picture_type == Review::PICTURE_TYPE_DUO) {
            $member = $donor->member_selection;
            $reviewDonor->setAttribute('gift_' . strtolower($member) . '_amount', 1);
        } else if ($donor->picture_type == Review::PICTURE_TYPE_SOLO) {
            $reviewDonor->gift_music_amount = 1;
        }

        $reviewDonor->presetGift($donor->level);

        if ($donor->is_required_gift == 1
            && $donor->take_gift == Review::TAKE_GIFT_YES) {
            $reviewDonor->to_send_gift = 1;
        } else {
            $reviewDonor->to_send_gift = 0;
        }

        $comments = [];

        if ($use == Review::USE_DONOR) {
            $transactions = $donor->transactions;
        } else if ($use == Review::USE_CONFIRMATION_DONOR) {
            $transactions = $donor->confirmationTransactions;
        }

        foreach ($transactions as $transaction) {
            $comments[] = $transaction->comment;
        }

        $reviewDonor->note = implode(',', $comments);
        if ($reviewDonor->save() == true) {
            $isValid = true;

            if (count($transactions) > 0) {

                foreach ($transactions as $transaction) {
                    $reviewTransaction = new ReviewTransaction();
                    $reviewTransaction->review_donor_id = $reviewDonor->id;
                    if ($transaction->valid == false) {
                        $isValid = false;
                    }
                    $reviewTransaction->value = $transaction->value;
                    $reviewTransaction->date = $transaction->date;
                    $reviewTransaction->method = $transaction->method;
                    $reviewTransaction->valid = $transaction->valid;
                    $reviewTransaction->comment = $transaction->comment;
                    if (!empty($transaction->comment)) {
                        $newComments[] = $transaction->comment;
                    }
                    $reviewTransaction->save();
                }

            } else {
                $isValid = false;
            }

            $reviewDonor->is_valid = $isValid;
            $reviewDonor->save();
        } else {
            print_r($reviewDonor->errors);
        }

        if (count($newComments) > 0) {
            $review->all_notes = $review->all_notes . ',' . implode(',', $newComments);
            $review->save();
        }
    }

    public static function syncByClass($class)
    {
        $reviews = Review::find()
            ->andWhere([
                'review_class' => $class
            ])
            ->all();

        if ($class == 'A' || $class == 'E') {
            foreach ($reviews as $review) {
                // FIX ME THERE SHOULD BE ONLY ONE CONFIRM
                $confirmation = Confirmation::find()
                    ->andFilterWhere([
                        'email' => $review->email
                    ])
                    ->one();

                $confirmationDonors = $confirmation->confirmationDonors;

                foreach ($confirmationDonors as $confirmationDonor) {
                    static::createReviewDonor($review, $confirmationDonor, Review::USE_CONFIRMATION_DONOR);
                }

                $review->review_process = 'reviewed';
                $review->is_review = 1;
                $review->save();
            }
        } else if ($class == 'B' || $class == 'F') {
            foreach ($reviews as $review) {
                $donors = Donor::find()
                    ->andFilterWhere([
                        'email' => $review->email
                    ])
                    ->all();

                foreach ($donors as $donor) {
                    static::createReviewDonor($review, $donor, Review::USE_DONOR);
                }

                $review->review_process = 'reviewed';
                $review->is_review = 1;
                $review->save();
            }
        } else if ($class == 'G' || $class == 'H') {
            foreach ($reviews as $review) {
                $donors = Donor::find()
                    ->andFilterWhere([
                        'email' => $review->email
                    ])
                    ->all();

                foreach ($donors as $donor) {
                    static::createReviewDonor($review, $donor, Review::USE_DONOR);
                }
            }
        } else if ($class == 'C'
            || $class == 'D'
            || $class == 'I'
            || $class == 'J'
            || $class == 'K'
            || $class == 'L') {
            foreach ($reviews as $review) {
                $donors = Donor::find()
                    ->andFilterWhere([
                        'email' => $review->email
                    ])
                    ->all();

                foreach ($donors as $donor) {
                    static::createReviewDonor($review, $donor, Review::USE_DONOR);
                }
            }
        }
    }

    public static function setRoundOnePhoto($class)
    {
        $reviews = Review::find()
            ->andWhere([
                'review_class' => $class,
                'round_number' => 1,
            ])
            ->all();

        foreach ($reviews as $review) {
            $donors = $review->reviewDonors;
            if (count($donors) > 0) {
                $donor = $donors[0];
                $donor->gift_round_1_photo_amount = '1';
                if ($donor->save() === false) {
                    echo '<pre>';
                    print_r($donor->errors);
                    echo '</pre>';
                }
            }
        }
    }

    public static function getGiftSummary()
    {
        $gifts = [
            'gift_round_1_photo_amount',
            'gift_picture_amount',
            'gift_wristband_amount',
            'gift_bag_amount',
            'gift_necklace_amount',
            'gift_keychain_amount',
            'gift_poster_a3_amount',
            'gift_poster_a4_amount',
            'gift_mini_photobook_amount',
            'gift_full_photobook_amount',
            'gift_polo_shirt_amount',
            'gift_polo_shirt_size_s_amount',
            'gift_polo_shirt_size_m_amount',
            'gift_polo_shirt_size_l_amount',
            'gift_polo_shirt_size_xl_amount',
            'gift_polo_shirt_size_2xl_amount',
            'gift_polo_shirt_size_3xl_amount',
            'gift_certificate_amount',
            'gift_akb_cd_amount',
            'gift_cherprang_amount',
            'gift_tarwaan_amount',
            'gift_jennis_amount',
            'gift_pupe_amount',
            'gift_noey_amount',
            'gift_kate_amount',
            'gift_jane_amount',
            'gift_namneung_amount',
            'gift_miori_amount',
            'gift_jaa_amount',
            'gift_kaew_amount',
            'gift_can_amount',
            'gift_mind_amount',
            'gift_orn_amount',
            'gift_namsai_amount',
            'gift_mobile_amount',
            'gift_music_amount',
            'gift_pun_amount',
            'gift_izurina_amount',
            'gift_satchan_amount',
            'gift_jib_amount',
            'gift_korn_amount',
            'gift_kaimook_amount',
            'gift_nink_amount',
            'gift_maysa_amount',
            'gift_piam_amount',
        ];

        $columns = [];
        foreach ($gifts as $gift) {
            $columns[] = new Expression('SUM(' . $gift . ') as ' . $gift);
        }

        $giftSum = ReviewDonor::find()
            ->select($columns)
            ->joinWith(['review r'], true, 'INNER JOIN')
            ->andWhere(['r.is_review' => true])
            ->andWhere(['to_send_gift' => true])
            ->all()[0];

        $reviewDonorModel = new ReviewDonor();

        $result = [];
        foreach ($gifts as $gift) {
            $result[$reviewDonorModel->getAttributeLabel($gift)] = $giftSum->$gift;
        }

        return $result;
    }

    public static function convertToSummary(array $donors)
    {
        $phones = ArrayHelper::map(Donation::find()
            ->asArray()
            ->all(), 'email', 'phone');

        $summary = [];

        $specialList = [
            'gift_round_1_photo_amount',
            'gift_polo_shirt_size_s_amount',
            'gift_polo_shirt_size_m_amount',
            'gift_polo_shirt_size_l_amount',
            'gift_polo_shirt_size_xl_amount',
            'gift_polo_shirt_size_2xl_amount',
            'gift_polo_shirt_size_3xl_amount',
        ];

        $giftList = [
            'gift_wristband_amount',
            'gift_bag_amount',
            'gift_necklace_amount',
            'gift_keychain_amount',
            'gift_poster_a3_amount',
            'gift_poster_a4_amount',
            'gift_mini_photobook_amount',
            'gift_full_photobook_amount',
            'gift_certificate_amount',
            'gift_akb_cd_amount',
        ];

        $pictureList = [
            'gift_cherprang_amount',
            'gift_tarwaan_amount',
            'gift_jennis_amount',
            'gift_pupe_amount',
            'gift_noey_amount',
            'gift_kate_amount',
            'gift_jane_amount',
            'gift_namneung_amount',
            'gift_miori_amount',
            'gift_jaa_amount',
            'gift_kaew_amount',
            'gift_can_amount',
            'gift_mind_amount',
            'gift_orn_amount',
            'gift_namsai_amount',
            'gift_mobile_amount',
            'gift_music_amount',
            'gift_pun_amount',
            'gift_izurina_amount',
            'gift_satchan_amount',
            'gift_jib_amount',
            'gift_korn_amount',
            'gift_kaimook_amount',
            'gift_nink_amount',
            'gift_maysa_amount',
            'gift_piam_amount',
        ];

        foreach ($donors as $donor) {
            $normalGifts = ReviewHelper::getGiftsByLevel($donor->level);
            if ($donor->receiving_method == Review::RECEIVING_METHOD_SELF) {
                $normalGifts['gift_poster_a4_amount'] = 1;
            }

            $gifts = [];

            if (!empty($donor->gift_wristband_number)) {
                $gifts[] = $donor->getAttributeCode('gift_wristband_number') . $donor->gift_wristband_number;
            }

            // Displays in case of existing
            foreach ($specialList as $specialItem) {
                $amount = intval($donor->$specialItem);
                if ($amount > 0) {
                    if ($amount > 1) {
                        $gifts[] = $donor->getAttributeCode($specialItem) . 'x' . $amount;
                    } else {
                        $gifts[] = $donor->getAttributeCode($specialItem);
                    }
                }
            }

            // Displays in case of exceeding normal amount
            $isNormal = true;
            foreach ($giftList as $giftItem) {
                $amount = intval($donor->$giftItem);
                if (isset($normalGifts[$giftItem])) {
                    if ($amount > $normalGifts[$giftItem]) {
                        $isNormal = false;
                        $gifts[] = $donor->getAttributeCode($giftItem) . 'x' . $amount;
                    } else if ($amount < $normalGifts[$giftItem]) {
                        $isNormal = false;
                        $gifts[] = $donor->getAttributeCode($giftItem) . 'x' . $amount;
                    }
                } else {
                    if ($amount > 0) {
                        $isNormal = false;
                        if ($amount > 1) {
                            $gifts[] = $donor->getAttributeCode($giftItem) . 'x' . $amount;
                        } else {
                            $gifts[] = $donor->getAttributeCode($giftItem);
                        }
                    }
                }
            }

            // Displays in case of existing
            foreach ($pictureList as $pictureItem) {
                $amount = intval($donor->$pictureItem);
                if ($amount > 0) {
                    if ($amount > 1) {
                        $gifts[] = $donor->getAttributeCode($pictureItem) . 'x' . $amount;
                    } else {
                        $gifts[] = $donor->getAttributeCode($pictureItem);
                    }
                }
            }

            $email = S::create($donor->review->email);
            $email = $email->collapseWhitespace();

            $fullName = S::create($donor->full_name);
            $fullName = $fullName->collapseWhitespace();

            $address = S::create($donor->address);
            $address = $address->replace($fullName, '');
            $address = $address->collapseWhitespace();

            $email = (string)$email;

            $summary[] = [
                'รหัสอ้างอิง' => $donor->id,
                'email' => $email,
                'ชื่อ' => (string)$fullName,
                'ที่อยู่' => (string)$address,
                'ระดับ' => $donor->level_text,
                'ของที่ระลึก' => $gifts,
                'โทร' => isset($phones[$email]) ? '0' . $phones[$email] : '',
                'กรณี' => $isNormal == true ? 'ปกติ' : 'พิเศษ',
                'ส่งหรือรับของที่ระลึกแล้ว' => $donor->gift_sent == true ? 'ใช่' : '',
                'วิธีการรับ' => $donor->receiving_method_text
            ];
        }

        return $summary;
    }

    public static function getAddressLabel($levelNumber = null, $receivingMethod = null)
    {
        $donors = ReviewDonor::find()
            ->joinWith(['review r'], true, 'INNER JOIN')
            ->where([
                'r.is_review' => true,
                'to_send_gift' => true,
                'receiving_method' => $receivingMethod == null ? Review::RECEIVING_METHOD_MAIL : $receivingMethod,
            ])
            ->andWhere([
                'IS NOT', 'level', null
            ])
            ->andFilterWhere([
                'level' => $levelNumber
            ])
            ->all();

        return static::convertToSummary($donors);
    }

    public static function getGiftsByLevel($levelNumber)
    {
        $gifts = [];
        switch ($levelNumber) {
            case 2:
                $gifts['gift_picture_amount'] = 1;
                break;
            case 3:
                $gifts['gift_picture_amount'] = 1;
                $gifts['gift_bag_amount'] = 1;
                break;
            case 4:
                $gifts['gift_picture_amount'] = 1;
                $gifts['gift_wristband_amount'] = 1;
                break;
            case 5:
                $gifts['gift_picture_amount'] = 1;
                $gifts['gift_bag_amount'] = 1;
                $gifts['gift_wristband_amount'] = 1;
                $gifts['gift_necklace_amount'] = 1;
                break;
            case 6:
                $gifts['gift_picture_amount'] = 1;
                $gifts['gift_bag_amount'] = 1;
                $gifts['gift_wristband_amount'] = 1;
                $gifts['gift_necklace_amount'] = 1;
                $gifts['gift_keychain_amount'] = 1;
                $gifts['gift_poster_a3_amount'] = 1;
                break;
            case 7:
                $gifts['gift_picture_amount'] = 1;
                $gifts['gift_bag_amount'] = 1;
                $gifts['gift_wristband_amount'] = 1;
                $gifts['gift_necklace_amount'] = 1;
                $gifts['gift_keychain_amount'] = 1;
                $gifts['gift_poster_a3_amount'] = 1;
                $gifts['gift_mini_photobook_amount'] = 1;
                break;
            case 8:
                $gifts['gift_picture_amount'] = 1;
                $gifts['gift_bag_amount'] = 1;
                $gifts['gift_wristband_amount'] = 1;
                $gifts['gift_necklace_amount'] = 1;
                $gifts['gift_keychain_amount'] = 1;
                $gifts['gift_poster_a3_amount'] = 1;
                $gifts['gift_polo_shirt_amount'] = 1;
                $gifts['gift_akb_cd_amount'] = 1;
                $gifts['gift_certificate_amount'] = 1;
                $gifts['gift_full_photobook_amount'] = 1;
                break;
            default:
                $gifts['gift_picture_amount'] = 0;
                $gifts['gift_bag_amount'] = 0;
                $gifts['gift_wristband_amount'] = 0;
                $gifts['gift_necklace_amount'] = 0;
                $gifts['gift_keychain_amount'] = 0;
                $gifts['gift_poster_a3_amount'] = 0;
                $gifts['gift_polo_shirt_amount'] = 0;
                $gifts['gift_akb_cd_amount'] = 0;
                $gifts['gift_certificate_amount'] = 0;
                $gifts['gift_full_photobook_amount'] = 0;
        }

        return $gifts;
    }

    public static function getUnsent()
    {
        $unsentDonors = ReviewDonor::find()
            ->joinWith(['review r'], true, 'INNER JOIN')
            ->where([
                'r.is_review' => true,
                'to_send_gift' => true,
            ])
            ->andWhere([
                'IS', 'gift_sent', null
            ])
            ->andWhere([
                'IS NOT', 'level', null
            ])
            ->all();

        return ReviewHelper::convertToSummary($unsentDonors);
    }
}